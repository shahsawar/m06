/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package activitat3_1;

import java.util.logging.Level;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;



/**
 *
 * @author shah
 */
public class MainHibernate {
    
    
    public static void main(String[] args){
        
        java.util.logging.Logger.getLogger("org.hibernate").setLevel(Level.WARNING);
        
        Client c1 = new Client(12341334, "TestHibernate", true);
    
        Configuration configuration = new Configuration();
        configuration.configure();
        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
        SessionFactory factory = configuration.buildSessionFactory(serviceRegistry);
        Session session = factory.openSession();
        
        try{
            session.beginTransaction ();
            session.save(c1);
            session.getTransaction().commit();
        
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            session.close();
            factory.close();
            StandardServiceRegistryBuilder.destroy(serviceRegistry);
        }
       
        
    }
    
    
    
}
