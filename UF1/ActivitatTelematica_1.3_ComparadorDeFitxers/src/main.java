import java.io.File;

public class main {

	// Comparar últimas modificaciones.
	public static String compararUltimaModificacion(File f1, File f2) {

		if (f1.lastModified() == f2.lastModified()) {
			return ("Tenen la mateixa data");
		} else if (f1.lastModified() > f2.lastModified()) {
			return ("El fitxer al directori A és més nou que a B");
		}
		return ("El fitxer al directori B és més nou que a A");
	}

	// Comparar tamaños de dos archivos
	public static String comprarMida(File f1, File f2) {

		if (f1.length() == f2.length()) {
			return ("Tenen la mateixa mida");
		} else if (f1.length() > f2.length()) {
			return ("El fitxer al directori A és més gran que a B");
		}
		return ("El fitxer al directori B és més gran que a A");
	}

	// Comparar el tamaño y la última modificacion de archivos.
	public static void mida_Y_UtModificacion(File f1, File f2) {

		if ((f1.length() == f2.length()) && (f1.lastModified() == f2.lastModified())) {
			System.out.println("I són exactament iguals en mida i data (de modificació)\n");
		} else {
			System.out.println(compararUltimaModificacion(f1, f2));
			System.out.println(comprarMida(f1, f2));
		}
	}

	public static void compararArchivos(String dir1, String dir2) {

		File dirA = new File(dir1);
		File dirB = new File(dir2);

		for (File f1 : dirA.listFiles()) {
			boolean existFile = false;
			for (File f2 : dirB.listFiles()) {
				if (f1.getName().equals(f2.getName())) {
					System.out.println("\n" + f1.getName() + " Existeix en ambdós directoris");
					mida_Y_UtModificacion(f1, f2);
					existFile = true;
				}
			}
			if (!existFile) {
				System.out.println("\n" + f1.getName() + " Existeix en el directori A, però no en el B");
			}
		}
	}

	public static void main(String[] args) {

		// Atributos
		// String rutaDirA = "/home/users/inf/wiam2/iam2318925/dirA";
		// String rutaDirB = "/home/users/inf/wiam2/iam2318925/dirB";

		String rutaDirA = "/home/shah/test/dir1";
		String rutaDirB = "/home/shah/test/dir2";

		compararArchivos(rutaDirA, rutaDirB);

	}

}
