import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

public class Client {

    // Attributes
    private String nif;
    private String nom;
    private int totalFacturacio;
    private String telefon;
    private String correu;
    List<Comanda> comandes = new ArrayList<>();

    // Constructor
    public Client(String nif, String nom, int totalFacturacio) {
        this.nif = nif;
        this.nom = nom;
        this.totalFacturacio = totalFacturacio;
    }

    // Convert document to client
    public Client(Document doc) {
        this.nif = doc.getString("nif");
        this.nom = doc.getString("nom");
        this.totalFacturacio = doc.getInteger("total_facturacio");

        if (doc.containsKey("telefon")) {
            this.telefon = doc.getString("telefon");
        } else {
            this.telefon = null;
        }

        if (doc.containsKey("correu")) {
            this.correu = doc.getString("correu");
        } else {
            this.correu = null;
        }

        if (doc.containsKey("comandes")) {
            // Load all document into list and then save the documents in comandes list.
            List<Document> listTmp = doc.getList("comandes", Document.class);

            for (Document documentTmp : listTmp) {
                comandes.add(new Comanda(documentTmp));
            }
        }
    }

    // Convert client to document
    public Document toDoc() {
        Document docTmp = new Document();
        docTmp.append("nif", nif);
        docTmp.append("nom", nom);
        docTmp.append("total_facturacio", totalFacturacio);

        if (telefon != null)
            docTmp.append("telefon", telefon);

        if (correu != null)
            docTmp.append("correu", correu);

        if (!comandes.isEmpty()) {
            List<Document> documentList = new ArrayList<>();
            for (Comanda comandaTmp : comandes) {
                documentList.add(comandaTmp.toDoc());
            }
            docTmp.append("comandes", documentList);
        }
        return docTmp;
    }


    //Getters and Setters
    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getTotalFacturacio() {
        return totalFacturacio;
    }

    public void setTotalFacturacio(int totalFacturacio) {
        this.totalFacturacio = totalFacturacio;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public String getCorreu() {
        return correu;
    }

    public void setCorreu(String correu) {
        this.correu = correu;
    }

    // Convert client document list to client list
    public static List<Client> loadIterable(Iterable<Document> iterable) {
        List<Client> clientsTmp = new ArrayList<>();
        for (Document documentTmp : iterable) {
            Client c = new Client(documentTmp);
            clientsTmp.add(c);
        }
        return clientsTmp;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("Nif: ").append(nif).append(System.lineSeparator());
        sb.append("Nom: ").append(nom).append(System.lineSeparator());
        sb.append("Total Facturació: ").append(totalFacturacio).append(System.lineSeparator());

        if (telefon != null)
            sb.append("Telèfon: ").append(telefon).append(System.lineSeparator());
        if (correu != null)
            sb.append("Correu: ").append(correu).append(System.lineSeparator());

        if (comandes.size() > 0){
            sb.append("Comandes: ").append(System.lineSeparator());
            for (Comanda c : comandes) {
                sb.append(c.toString());
            }
        }
        return sb.toString();
    }
}
