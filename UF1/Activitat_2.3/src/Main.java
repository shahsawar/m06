import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;

public class Main {

	public static String getRuta() {
		String ruta = "/home/users/inf/wiam2/iam2318925/TestM06/file1";
		return ruta;
	}

	public static Scanner getScanner() {
		Scanner sc = new Scanner(System.in);
		return sc;
	}

	public static Treballador askForData() {
		
		Treballador t = new Treballador();
		
		System.out.println("Introdueix el NIF :");
		t.NIF = getScanner().next();
		System.out.println("Introdueix el Nom :");
		t.Nom = getScanner().next();
		System.out.println("Introdueix el Sou :");
		t.Sou = getScanner().nextInt();
		
		return t;
	}

	public static void insertDataV1() {

		// Escribir el Objeto en un archivo
		try (ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(getRuta()))) {
			outputStream.writeObject(askForData());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void readDataV1() {

		Treballador t = null;

		try (ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(getRuta()))) {
			t = (Treballador) inputStream.readObject();
			inputStream.close();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}

		System.out.println("Dades del treballador: ");
		System.out.println("NIF : " + t.NIF);
		System.out.println("NOM : " + t.Nom);
		System.out.println("SOU : " + t.Sou);
	}

	public static void insertDataV3() {

		try (DataOutputStream outPut = 
				new DataOutputStream(new BufferedOutputStream(new FileOutputStream(getRuta())))){
			Treballador t = askForData();
			
			outPut.writeUTF(t.NIF);
			outPut.writeUTF(t.Nom);
			outPut.writeInt(t.Sou);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {

		System.out.print("Seleciona una opció: \n1.Emmagatzemar Dades\n2.Recuperar dades\n:");
		int op = getScanner().nextInt();

		if (op == 1) {
			insertDataV3();
		} else {
			readDataV1();
		}

		/*
		 * Versió 2 Afegiu ara a la classe "Treballador" un camp edat, que sigui un
		 * enter. Proveu a executar el programa recuperant les dades desades amb la
		 * versió anterior. És compatible?
		 * 
		 * Em dona error, no es compatible.
		 */

	}

}
