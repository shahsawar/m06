package org.gitlab.shahsawar;

import org.gitlab.shahsawar.Politic;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;

public class PoliticMain {

    // Scanner
    public static Scanner getScanner() {
        Scanner sc = new Scanner(System.in);
        return sc;
    }

    // Return date in sql format
    public static Date setDate(String strDate) {
        LocalDate date = LocalDate.parse(strDate);
        return Date.valueOf(date);
    }

    public static void main(String[] args) {

        //Show only warning info
        java.util.logging.Logger.getLogger("org.hibernate").setLevel(Level.WARNING);

        // Initialize connection
        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().configure().build();
        SessionFactory sessionFactory = new MetadataSources(serviceRegistry).buildMetadata().buildSessionFactory();

        // Exercici1: Programa i prova un mètode que creï la taula politic.
        Politic.createTable(ConnDB.startConn());

        //Exercici2: Afegeix una classe "Politic" que representi la taula anterior.
        Politic politic = new Politic("12345678M", "Politic1", setDate("2020-02-02"), 100, false);
        politic.add(sessionFactory);

        // Exercici3: Programa un mètode que demani les 5 dades (nif, nom, dataNaixement, sou, esCorrupte) i que retorni un objecte de la classe Politic.
        // Programa també un mètode que, donat l'objecte de la classe Politic, insereixi el polític a la taula. És recomanable fer-ho amb concatenació de valors.
        // Comprova que totes dues coses funcionen correctament: que es demanen a l'usuari les dades i s'afegeix a la taula.
        Politic.read().add(sessionFactory);

        //Exercici 4:
        //Programa un mètode que llegeixi de la taula totes les files i construeixi una llista d'objectes de la classe Politic a partir de les files que hi ha a la taula.
        //Programa un mètode que donat un objecte de la classe Politic imprimeixi per pantalla les seves dades (o bé sobreescriu el mètode toString a la classe Politic).
        //Comprova que la recuperació ha sigut correcta pintant per pantalla tots els objectes de la classe Politic que has recuperat.
        List<Politic> politicList = Politic.readFromDB(sessionFactory);
        Politic.print(politicList.get(1));


        // Close all connections.
        sessionFactory.close();
        StandardServiceRegistryBuilder.destroy(serviceRegistry);
    }
}
