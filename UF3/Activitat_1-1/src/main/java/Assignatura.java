import org.bson.Document;

public class Assignatura {

    private String nom;
    private int hores_setmana;

    public Assignatura(String nom, int hores_setmana) {
        this.nom = nom;
        this.hores_setmana = hores_setmana;
    }

    public Assignatura(Document document) {
        this.nom = document.getString("nom");
        this.hores_setmana = document.getInteger("hores_setmana");
    }


    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getHores_setmana() {
        return hores_setmana;
    }

    public void setHores_setmana(int hores_setmana) {
        this.hores_setmana = hores_setmana;
    }

    //Convert to document
    public Document toDocument() {
        Document doc = new Document("nom", nom).append("hores_setmana", hores_setmana);
        return doc;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Assignatura {").append(System.lineSeparator());
        sb.append("\tnom: ").append(nom).append(System.lineSeparator());
        sb.append("\thores_setmana: ").append(hores_setmana).append(System.lineSeparator()).append("}");
        return String.valueOf(sb);
    }
}
