package activitat1_2;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Scanner;

public class Client {

	protected int dni;
	protected String nom;
	protected boolean premium;
	HashSet<Comanda> comandes;

	public Client() {
		dni = 0;
		nom = null;
		premium = false;
		comandes = new HashSet<Comanda>();
	}

	public Client(int dni, String nom, boolean premium) {
		this.dni = dni;
		this.nom = nom;
		this.premium = premium;
		comandes = new HashSet<Comanda>();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + dni;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Client other = (Client) obj;
		if (dni != other.dni)
			return false;
		return true;
	}

	// Scanner
	public static Scanner getScanner() {
		Scanner sc = new Scanner(System.in);
		return sc;
	}

	// Creació de taula client en mysql
	public static void createTable(Connection con) throws SQLException {
		if (!isTableExists(con)) {
			String sentenciaSQL = "CREATE TABLE client (" + "dni INT(8)," + "nom VARCHAR(20),"
					+ "premium ENUM('S','N')," + "PRIMARY KEY (dni)" + ")";
			Statement statement = con.createStatement();
			statement.execute(sentenciaSQL);
			System.out.println("Taula client creada.");
		} else {
			System.err.println("ERROR: La taula client ja existeix!");
		}
	}

	// Comprovar si la taula existeix a la base de dades
	public static boolean isTableExists(Connection con) {
		try {
			DatabaseMetaData dbm = con.getMetaData();
			ResultSet tables = dbm.getTables(null, null, "client", null);
			if (tables.next()) {
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	// Recuperació de les dades de la BBDD
	public static HashSet<Client> read(Connection con) throws SQLException {

		HashSet<Client> clients = new HashSet<Client>();
		String sentenciaSQL = "select * from client";
		Statement statement = con.createStatement();
		ResultSet rs = statement.executeQuery(sentenciaSQL);

		while (rs.next()) {
			Client clientTmp = new Client();
			clientTmp.dni = rs.getInt("dni");
			clientTmp.nom = rs.getString("nom");
			clientTmp.premium = rs.getString("premium").equalsIgnoreCase("S");

			String sentenciaSqlComanda = "select * from comanda where dni_client='" + clientTmp.dni + "'";
			Statement statementComanda = con.createStatement();
			ResultSet rsComanda = statementComanda.executeQuery(sentenciaSqlComanda);

			// Comandes de client actual
			while (rsComanda.next()) {
				Comanda comandaTmp = new Comanda();
				comandaTmp.numComanda = rsComanda.getInt("num_comanda");
				comandaTmp.preuTotal = rsComanda.getDouble("preu_total");
				comandaTmp.data = rsComanda.getDate("data").toLocalDate();
				clientTmp.comandes.add(comandaTmp);
			}
			clients.add(clientTmp);
		}
		return clients;
	}

	// Emmagatzemat de dades a la BBDD
	public void save(Connection con) {
		String sentenciaSQL = "insert into client (dni, nom, premium) values(?,?,?)";
		try {
			PreparedStatement sentenciaPreparada = con.prepareStatement(sentenciaSQL);
			sentenciaPreparada.setInt(1, dni);
			sentenciaPreparada.setString(2, nom);
			sentenciaPreparada.setString(3, ((premium) ? "S" : "N"));
			sentenciaPreparada.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	// Alta d'un nou client
	public static Client newClient() {

		// Demanar dades al client
		System.out.print("Introdueix el DNI: ");
		int dni = getScanner().nextInt();
		System.out.print("Introdueix el NOM: ");
		String nom = getScanner().next();
		System.out.print("És premium? (s/n): ");
		boolean esPremium = getScanner().next().equalsIgnoreCase("s");

		// Crear un objecte client amb les dades introduïdes i tornar-lo.
		Client clientTmp = new Client(dni, nom, esPremium);
		return clientTmp;
	}

	// Mostrar comandes client
	public static void showComandes(HashSet<Client> clients) {

		// Demanar dades del client
		String nomClient = printDniNom(clients);

		if (nomClient != "Client no existeix!") {
			for (Client client : clients) {
				if (nomClient == client.nom) {
					System.out.println("\nComandes: ");
					for (Comanda c : client.comandes) {
						c.print();
					}
				}
			}
		} else {
			System.out.println(nomClient);
		}
	}

	// Imprimir per pantalla el nom i dni de tots els clients.
	public static String printDniNom(HashSet<Client> clients) {

		int cont = 1;
		System.out.println("\nDades del Client: ");
		for (Client client : clients) {
			System.out.println((cont++) + ") " + client.dni + " - " + client.nom);
		}

		System.out.print("\nSelecciona el client: ");
		int op = getScanner().nextInt();
		cont = 0;
		for (Client client : clients) {
			cont++;
			if (op == cont) {
				return client.nom;
			}
		}
		return "Client no existeix!";
	}

	// Imprimir per pantalla el nom i dni de tots els clients.
	public static int getDni(HashSet<Client> clients) {

		int cont = 1;
		System.out.println("\nDades del Client: ");
		for (Client client : clients) {
			System.out.println((cont++) + ") " + client.dni + " - " + client.nom);
		}

		System.out.print("\nSelecciona el client: ");
		int op = getScanner().nextInt();
		cont = 0;
		for (Client client : clients) {
			cont++;
			if (op == cont) {
				return client.dni;
			}
		}
		return 0;
	}

	// Eliminar totes les dades de la taula
	public static void deleteTablesData(Connection con, String table) {
		String sentenciaSQL = "delete from " + table + "";
		try {
			PreparedStatement preparedStatement = con.prepareStatement(sentenciaSQL);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	// Eliminació d'un client. Se selecciona el client i s'elimina el client i totes
	// les seves comandes de la BBDD
	public static void deleteClientData(Connection con, int dni) {
		String sentenciaSQL = "delete from comanda where dni_client = " + dni + "";
		String sentenciaSQL2 = "delete from client where dni = " + dni + "";
		try {
			PreparedStatement preparedStatement = con.prepareStatement(sentenciaSQL);
			preparedStatement.executeUpdate();

			preparedStatement = con.prepareStatement(sentenciaSQL2);
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void updateData(Connection con, int dni) {

		System.out.print("Nom nou:");
		String newName = getScanner().next();

		System.out.println("És premium?");
		Boolean isPremium = getScanner().next().equalsIgnoreCase("s");

		String sentenciaSQL = "update client set nom = ?, premium = ? where dni=? ";
		try {
			PreparedStatement sentenciaPreparada = con.prepareStatement(sentenciaSQL);
			sentenciaPreparada.setString(1, newName);
			sentenciaPreparada.setString(2, ((isPremium) ? "S" : "N"));
			sentenciaPreparada.setInt(3, dni);
			sentenciaPreparada.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void showName(Connection con) {
		System.out.print("Mostrar per pantalla els clients que el seu nom comenci per:");
		String letters = getScanner().next();
		try {
			String sentenciaSQL = "select * from client where nom LIKE Concat('" + letters + "','%')";
			Statement statement = con.createStatement();
			ResultSet rs = statement.executeQuery(sentenciaSQL);
			while (rs.next()) {
				System.out.println(rs.getString("nom"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public static void GenerationOfBilling(Connection con) {

		System.out.print("Introduïu un mes: ");
		int month = getScanner().nextInt();

		System.out.println("Introduïu un any: ");
		int year = getScanner().nextInt();

		if ((month > 0 && month <= 12) && ((Integer.toString(year).length() == 4) && year > 0)) {

			try {
				con.setAutoCommit(false);
				String query = "{CALL crea_resum_facturacio(?, ?)}";
				CallableStatement stmt = con.prepareCall(query);
				stmt.setInt(1, month);
				stmt.setInt(2, year);
				stmt.executeQuery();
				con.commit();
			} catch (SQLException e) {
				System.err.println("S'ha produït un error a l'hora de crear resum de facturació!");
				e.printStackTrace();
				try {
					con.rollback();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		} else {
			System.err.println("El format de mes o any és incorrecte!");
		}
	}

}
