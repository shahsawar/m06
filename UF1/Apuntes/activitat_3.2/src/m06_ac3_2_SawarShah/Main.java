package m06_ac3_2_SawarShah;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import javax.xml.xpath.XPathExpression;


public class Main {
	
	// Scanner
	public static Scanner getScanner() {
		Scanner sc = new Scanner(System.in);
		return sc;
	}
	
	
	// Menu
	public static int menu() {
		System.out.println(
				"0 - Sortir\n"+
				"1 - Llegir d'un fitxer XML pel mètode seqüencial\n"+
				"2 - Llegir d'un fitxer XML pel mètode sintàctic\n"+
				"3 - Mostrar per pantalla totes les assignatures amb les seves dades (número, nom, durada i la llista d'alumnes\n"+
				"4 - Afegir una assignatura\n"+
				"5 - Afegir un alumne a una assignatura\n"+
				"6 - Guardar a disc en XML amb les assignatures\n");
		int op = getScanner().nextInt();
		return op;
	}
	
	
	// Mostrar per pantalla totes les assignatures amb les seves dades (número, nom,
	// durada i la llista d'alumnes)
	public static void imprimirDades(List<Assignatura> assignatura) {
		for (Assignatura a : assignatura) {
			a.imprimir();
		}
	}

	
	// Comprovant si el fitxer existeix i que no sigui el directori.
	public static Boolean isFileExists(String fileRuta) {
		File f = new File(fileRuta);
		if (f.exists() && !f.isDirectory()) {
			return true;
		}
		return false;
	}
	
	
	
	// Comprovar si existeix l'assignatura
	public static boolean comprovarAssignatura(List<Assignatura> assignatura, int num) {
		for (Assignatura a : assignatura) {
			if (a.numero == num) {
				return true;
			}
		}
		return false;
	}
	
	
	// Demana el nom de l'arxiu
	public static String demanarNomFitxer() {
		System.out.println("Introdueix el nom de fitxer (Ex: assignatura.xml): ");
		String rutaFitxer = getScanner().nextLine();
		return rutaFitxer;
	}

	
	// Afegir una assignatura
	public static void afegirAssignatura(List<Assignatura> assignatura) {

		System.out.println("Introdueix el nombre d'assignatura: ");
		int numero = getScanner().nextInt();

		System.out.println("Introdueix el nom d'assignatura: ");
		String nom = getScanner().nextLine();

		System.out.println("Introdueix la durada d'assignatura: ");
		int durada = getScanner().nextInt();

		Assignatura assignaturaTmp = new Assignatura(numero, nom, durada);
		assignatura.add(assignaturaTmp);
	}

	
	// Afegir un alumne a una assignatura
	public static void afegirAlumneAssignatura(List<Assignatura> assignatura) {

		System.out.println("Introdueix el nombre d'assignatura: ");
		int numero = getScanner().nextInt();

		// Si l'assignatura existeix
		if (comprovarAssignatura(assignatura, numero)) {

			System.out.println("Introdueix el nom del alumne: ");
			String nom = getScanner().nextLine();

			System.out.println("Introdueix el DNI de l'alumne: ");
			String dni = getScanner().nextLine();

			System.out.println("¿L'alumne ha repetit el curs? (true/false)");
			Boolean repetidor = getScanner().nextBoolean();

			Alumne alumneTmp = new Alumne(nom, dni, repetidor);
			assignatura.get(numero - 1).alumnes.add(alumneTmp);
			
		} else {
			System.err.println("No existeix cap assignatura amb aquest número!");
		}
	}
	
	
	
	// Guardar a disc en XML amb les assignatures
	public static void desarAdisc(List<Assignatura> assignaturas) {

		try {
			// Creant un nou document
			Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();

			// Tag assignaturas
			Element nodeAssignaturas = doc.createElement("assignaturas");
			doc.appendChild(nodeAssignaturas);

			// Per cada assignatura
			for (Assignatura assignatura : assignaturas) {
				assignatura.desar_xml(nodeAssignaturas, doc);
			}

			// Per desar la informació en un arxiu.
			TransformerFactory.newInstance().newTransformer().transform(new DOMSource(doc),
					new StreamResult(new File("assignatura.xml")));

		} catch (ParserConfigurationException | TransformerException | TransformerFactoryConfigurationError e) {
			e.printStackTrace();
		}
	}
	
	
	// Llegir d'un fitxer XML pel mètode seqüencial 
	public static void llegirSeq(List<Assignatura> Assignaturas) {

		String rutaFitxer = demanarNomFitxer();

		if (isFileExists(rutaFitxer)) {
			try {
				Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(rutaFitxer);
				Node arrel = doc.getDocumentElement();
				NodeList llistaNodes = arrel.getChildNodes();

				for (int i = 0; i < llistaNodes.getLength(); i++) {

					if (llistaNodes.item(i).getNodeName().equals("assignatura")) {
						Assignatura assignatura = new Assignatura();
						assignatura.llegirXmlSeq(llistaNodes.item(i).getChildNodes());
						Assignaturas.add(assignatura);
					}
				}
			} catch (Exception e) {
			}
		} else {
			System.err.println("L'arxiu no existeix!");
		}
	}
	
	
	// Llegir d'un fitxer XML pel mètode sintàctic
	public static void llegirSin(List<Assignatura> Assignaturas) {
		
		String rutaFitxer = demanarNomFitxer();
		
		if (isFileExists(rutaFitxer)) {
			try {
				Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(rutaFitxer);
				XPathExpression expr = XPathFactory.newInstance().newXPath().compile("/assignaturas/assignatura");
				NodeList nodeList = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);

				for (int i = 0; i < nodeList.getLength(); i++) {
					Assignatura assignatura = new Assignatura();
					assignatura.llegirXmlSin(doc, expr, i);
					Assignaturas.add(assignatura);
				}
			} catch (Exception e) {
			}
		} else {
			System.err.println("L'arxiu no existeix!");
		}
	}
	
	
	public static void main(String[] args) throws ParserConfigurationException, TransformerConfigurationException, TransformerException, TransformerFactoryConfigurationError, SAXException, IOException, XPathExpressionException {
		
		List<Assignatura> assignaturas = new ArrayList<>();
		
		int option = menu();
		while (option != 0) {

			switch (option) {
			case 1:
				llegirSeq(assignaturas);
				break;

			case 2:
				llegirSin(assignaturas);
				break;

			case 3:
				imprimirDades(assignaturas);
				break;

			case 4:
				afegirAssignatura(assignaturas);
				break;
			
			case 5:
				afegirAlumneAssignatura(assignaturas);
				break;
				
			case 6:
				desarAdisc(assignaturas);
				break;
				
			default:
				System.err.println("Aquesta opció no és vàlida");
				break;
			}
			option = menu();
		}
		System.out.println("Surt del programa.");

		
		/*
		//Creando asignaturas
		Assignatura cat = new Assignatura(1, "Català", 1);
		Assignatura cas = new Assignatura(2, "Castellà", 2);
		Assignatura ang = new Assignatura(3, "Anglès", 10);
		
		
		//Creando alumnos
		Alumne a1 = new Alumne("alumne1", "12345678F", false);
		Alumne a2 = new Alumne("alumne2", "12345628F", true);
		Alumne a3 = new Alumne("alumne3", "12345658F", false);
		Alumne a4 = new Alumne("alumne4", "12325628F", true);
		Alumne a5 = new Alumne("alumne5", "12335658F", false);
		
		
		//Asignar asignatura al alumno.
		cat.alumnos.add(a1);
		cas.alumnos.add(a2);
		ang.alumnos.add(a3);
		ang.alumnos.add(a4);
		ang.alumnos.add(a5);
		
		
		//Agregar las asignaturas en la lista asignaturas.
		assignaturas.add(cat);
		assignaturas.add(cas);
		assignaturas.add(ang);
		*/
	}
	
}