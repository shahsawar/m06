import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Scanner;

public class Main {

	
	// Método que pinta por pantalla el contenido de un directorio
	public static void printDirectory() {
		String ruta = "/home/shahsawar/test";
		File dir = new File(ruta);
		System.out.println(dir.getAbsolutePath());
		for (String c : dir.list()) {
			System.out.println(c);
		}
	}

	
	//Función que elimina archivos o directorios.
	public static void deleteFile(File f) {	
		if (f.delete()) {
			System.out.println("S'ha esborrat el fitxer "+f.getName());
		} else {
			System.out.println("Error a l'esborrar el fitxer "+f.getName());
		}
	}
	public static void deleteFileMain() {	
		String ruta = "/home/shahsawar/test";
		File dir = new File(ruta);
		//Lista todo el contenido del directorio dir.
		for (File f : dir.listFiles()) {
			deleteFile(f);
		}
		deleteFile(dir);//Por ultimo elimino el directorio.
	}

	
	
	//Exercicio 1.3 comparador de ficheros.
	
	// Comparar últimas modificaciones.
	public static String compararUltimaModificacion(File f1, File f2) {
		if (f1.lastModified() == f2.lastModified()) {
			return ("Tenen la mateixa data");
		} else if (f1.lastModified() > f2.lastModified()) {
			return ("El fitxer al directori A és més nou que a B");
		}
		return ("El fitxer al directori B és més nou que a A");
	}

	// Comparar tamaños de dos archivos
	public static String comprarMida(File f1, File f2) {

		if (f1.length() == f2.length()) {
			return ("Tenen la mateixa mida");
		} else if (f1.length() > f2.length()) {
			return ("El fitxer al directori A és més gran que a B");
		}
		return ("El fitxer al directori B és més gran que a A");
	}

	// Comparar el tamaño y la última modificacion de archivos.
	public static void mida_Y_UtModificacion(File f1, File f2) {

		if ((f1.length() == f2.length()) && (f1.lastModified() == f2.lastModified())) {
			System.out.println("I són exactament iguals en mida i data (de modificació)\n");
		} else {
			System.out.println(compararUltimaModificacion(f1, f2));
			System.out.println(comprarMida(f1, f2));
		}
	}

	public static void compararArchivos(String dir1, String dir2) {

		File dirA = new File(dir1);
		File dirB = new File(dir2);

		for (File f1 : dirA.listFiles()) {
			boolean existFile = false;
			for (File f2 : dirB.listFiles()) {
				if (f1.getName().equals(f2.getName())) {
					System.out.println("\n" + f1.getName() + " Existeix en ambdós directoris");
					mida_Y_UtModificacion(f1, f2);
					existFile = true;
				}
			}
			if (!existFile) {
				System.out.println("\n" + f1.getName() + " Existeix en el directori A, però no en el B");
			}
		}
	}
	
	
	// MÈTODES ORIENTATS A BYTES
	public static void metodoOrientadoABytes() {
		String ruta = "/home/shahsawar/Documents/Entregas/testM06/file";
		try {
			DataOutputStream streamEscriptura = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(ruta)));
			DataInputStream streamLectura = new DataInputStream(new BufferedInputStream(new FileInputStream(ruta)));
			
			// Escriptura
			streamEscriptura.writeBoolean(true);
			streamEscriptura.writeInt(1000000);
			streamEscriptura.writeUTF("Proba ejercici1");
			streamEscriptura.close();

			// Lectura
			boolean valor1 = streamLectura.readBoolean();
			int valor2 = streamLectura.readInt();
			String valor3 = streamLectura.readUTF();

			System.out.println("El valor booleano: "+valor1);
			System.out.println("El valor entero: "+valor2);
			System.out.println("Conjunto de caracteres: "+valor3);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	//MÈTODES ORIENTATS A CARÀCTERS
	
	//Escriptura
	OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream("fitxerProva.txt"), "ISO-8859-1");
	writer.write("Text de prova");

	//Lectura
	InputStreamReader reader= new InputStreamReader(new FileInputStream("fitxerProva.txt"),"ISO-8859-1");
	int charsLlegits=0;
	char[] buffer = new char[1000];
	while(charsLlegits!=-1){
		charsLlegits=reader.read(buffer);
	} 
	
	//Cadenas separadas por @
	char[] buffer = newchar[1000];
	String acumulador;
	while(charsLlegits!=-1){
		charsLlegits=reader.read(buffer);
		acumulador += String.valueOf(buffer);
	}
	String[] separats = acumulador.split("@");
	System.out.println(separats[0]);
	
	
	
	//Métode Seriació
	//Utilitzem la interfície "Serializable" 
	public static Treballador askForData() {
		Treballador t = new Treballador();
		
		System.out.println("Introdueix el NIF :");
		t.NIF = getScanner().next();
		System.out.println("Introdueix el Nom :");
		t.Nom = getScanner().next();
		System.out.println("Introdueix el Sou :");
		t.Sou = getScanner().nextInt();
		
		return t;
	}

	public static void insertDataV1() {

		// Escribir el Objeto en un archivo
		try (ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(getRuta()))) {
			outputStream.writeObject(askForData());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void readDataV1() {
		Treballador t = null;

		try (ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(getRuta()))) {
			t = (Treballador) inputStream.readObject();
			inputStream.close();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}

		System.out.println("Dades del treballador: ");
		System.out.println("NIF : " + t.NIF);
		System.out.println("NOM : " + t.Nom);
		System.out.println("SOU : " + t.Sou);
	}
	
	//Forma binario específico
	public static void insertDataV3() {

		try (DataOutputStream outPut = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(getRuta())))){
			Treballador t = askForData();
			
			outPut.writeUTF(t.NIF);
			outPut.writeUTF(t.Nom);
			outPut.writeInt(t.Sou);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	
	// Fichero properties
	// Llegir dades d'habitació d'l'arxiu.
	Properties p = new Properties();
	try (DataInputStream ip = new DataInputStream(new FileInputStream("dadesHabitacio.properties"))) {
		p.load(ip);
		ample = Double.parseDouble(p.getProperty("Ample"));
		llarg = Double.parseDouble(p.getProperty("Llarg"));
		nom = p.getProperty("Nom");
	} catch (IOException e) {
		e.printStackTrace();
	}

	
	 //Creació d'arxiu de propietat i guardar els valors predeterminats. 
	 Properties p = new Properties();
	  
	 try (DataOutputStream op = new DataOutputStream(new
	 FileOutputStream("dadesHabitacio.properties"))){
	  
		 p.setProperty("Ample", "200"); p.setProperty("llarg", "200");
		 p.setProperty("Nom", "N2I");
  
		 p.store(op, null);
  
	 } catch (IOException e) { e.printStackTrace(); } }
 

	

	
	 
	
	public static void main(String[] args) {
		
		
		
		
		//printDirectory();

		
		/*
		String rutaDirA = "/home/shah/test/dir1";
		String rutaDirB = "/home/shah/test/dir2";
		compararArchivos(rutaDirA, rutaDirB);*/

		
		
	}

}
