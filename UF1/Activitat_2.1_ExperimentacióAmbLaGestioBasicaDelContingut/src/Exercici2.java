import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class Exercici2 {

	public static void main(String[] args) {

		// Ruta
		//String ruta = "/home/users/inf/wiam2/iam2318925/TestM06/exercici2";
		String ruta = "/home/shahsawar/Documents/Entregas/testM06/file2";
		
		try {
			OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(ruta), "UTF-8");
			InputStreamReader reader = new InputStreamReader(new FileInputStream(ruta), "UTF-8");
			
			
			// Escriptura
			writer.write(215);
			writer.write("@");//Separador
			writer.write("Aquesta és la pràctica 3 (Mañana, ç)");
			writer.close();

			// Lectura
			/*int charsLlegits = 0;
			char[] buffer = new char[1000];
			while (charsLlegits != -1) {
				charsLlegits = reader.read(buffer);
				System.out.println(String.valueOf(buffer));
			}*/
			
			int charsLlegits = 0;
			char[] buffer = new char[1000];
			String acumulador = "";
			
			while(charsLlegits != -1){
				charsLlegits=reader.read(buffer);
				acumulador += String.valueOf(buffer);
			}
			String[] separats = acumulador.split("@");
			System.out.println(separats[0]);
			System.out.println(separats[1]);
			
			reader.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	
	/*
	 * Observa el contingut. Quina diferència hi ha amb l’anterior? 
	 * Com pots distingir quan acaba un i comença l’altre?
	 * 
	 * Quan escric un nombre enter, el seu valor equivalent en utf8 es guarda a l'arxiu
	 * 
	 * */
	
}
