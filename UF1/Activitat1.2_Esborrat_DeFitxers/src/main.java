import java.io.File;


public class main {

	
	//Función que elimina archivos o directorios.
	public static void deleteFile(File f) {
		
		if (f.delete()) {
			System.out.println("S'ha esborrat el fitxer "+f.getName());
		}else {
			System.out.println("Error a l'esborrar el fitxer "+f.getName());
		}
	}
	
	
	public static void main(String[] args) {
		
		//String ruta = "/home/users/inf/wiam2/iam2318925/DIRTEST/";
		String ruta = "/home/shah/Documents/gradosuperior/M06/UF1/Activitat1.2_Esborrat_DeFitxers/DIRTEST";
		File dir = new File(ruta);
		
		//Lista todo el contenido del directorio dir.
		for (File f : dir.listFiles()) {
			deleteFile(f);
		}
		
		deleteFile(dir);
	}
}
