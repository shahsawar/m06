public class Llibre {

    private String signatura;
    private String nom;
    private Boolean prestec;
    private String localizacio;

    public Llibre(String signatura, String nom, Boolean prestec, String localizacio) {
        this.signatura = signatura;
        this.nom = nom;
        this.prestec = prestec;
        this.localizacio = localizacio;
    }

    public String getSignatura() {
        return signatura;
    }

    public void setSignatura(String signatura) {
        this.signatura = signatura;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Boolean getPrestec() {
        return prestec;
    }

    public void setPrestec(Boolean prestec) {
        this.prestec = prestec;
    }

    public String getLocalizacio() {
        return localizacio;
    }

    public void setLocalizacio(String localizacio) {
        this.localizacio = localizacio;
    }
}
