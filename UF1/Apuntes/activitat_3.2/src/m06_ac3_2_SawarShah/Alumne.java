package m06_ac3_2_SawarShah;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

public class Alumne {

	// Atributs
	public String nom;
	public String dni;
	public Boolean repetidor;

	
	// Constructor buit
	public Alumne() {}
	
	
	// Constructor con parámetros
	public Alumne(String nom, String dni, Boolean repetidor) {
		this.nom = nom;
		this.dni = dni;
		this.repetidor = repetidor;
	}

	
	// Imprimir dades per pantalla
	public void imprimir() {
		System.out.println("Nom: " + nom);
		System.out.println("Dni: " + dni);
		System.out.println("Repetidor: " + repetidor);
	}

	
	// En XML en lloc de guardar true o false, guardo si o no.
	public String esRepetidor() {
		if (repetidor) return "Si";
		return "No";
	}

	
	// Guardar en disc els alumnes.
	public void desar_xml(Element nodeAssignatura, Document doc) {
		
		//Guardar informació d'alumne com atributs
		Element nodeAlumno = doc.createElement("alumne");
		nodeAlumno.setAttribute("nom", nom);
		nodeAlumno.setAttribute("dni", dni);
		nodeAlumno.setAttribute("repetidor", esRepetidor());
		nodeAssignatura.appendChild(nodeAlumno);
		
		/*
		// Guardar informació d'alumne en elements
		Element nodeNom = doc.createElement("nom");
		nodeNom.setTextContent(nom);
		nodeAssignatura.appendChild(nodeNom);
		
		Element nodeDni = doc.createElement("dni");
		nodeNom.setTextContent(nom);
		nodeAssignatura.appendChild(nodeDni);
		
		Element nodeRepetidor = doc.createElement("repetidor");
		if (repetidor) {
			nodeRepetidor.setTextContent("Si");
		}else {
			nodeRepetidor.setTextContent("No");
		}*/		
	}
	
	
	// Llegir els estudiant de el disc.
	public void llegir_xml(Node nodeAlumne) {
		
		//Guardar los attributos de node alumne en namedNodeMap
		NamedNodeMap attibuts = nodeAlumne.getAttributes();
		this.dni = attibuts.getNamedItem("dni").getTextContent();
		this.nom = attibuts.getNamedItem("nom").getTextContent();
		
		if (attibuts.getNamedItem("repetidor").getTextContent().equals("Si")) {
			this.repetidor = true;
		}else {
			this.repetidor = false;
		}
	}
}