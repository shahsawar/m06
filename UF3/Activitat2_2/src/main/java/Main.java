import com.mongodb.client.*;
import com.mongodb.client.model.Filters;
import org.bson.Document;
import static com.mongodb.client.model.Filters.gt;

import java.util.ArrayList;
import java.util.List;


public class Main {

    public static void main(String[] args) {

        try(MongoClient mongoClient = MongoClients.create()){
            MongoDatabase database = mongoClient.getDatabase("bd_prova");
            MongoCollection<Document> collection = database.getCollection("assignatures");
            exercici2(collection);
        }
    }


    public static void exercici1(MongoCollection collection){

        List<Document> assignaturaList = new ArrayList<>();

        Assignatura assignatura1 = new Assignatura("M06", 5);
        Assignatura assignatura2 = new Assignatura("M07", 5);
        Assignatura assignatura3 = new Assignatura("M08", 5);

        Alumne a1 = new Alumne("12345678X", "alumno1");
        Alumne a2 = new Alumne("22345678X", "alumno2");
        Alumne a3 = new Alumne("32345678X", "alumno3");
        Alumne a4 = new Alumne("42345678X", "alumno4");
        Alumne a5 = new Alumne("52345678X", "alumno5");
        Alumne a6 = new Alumne("62345678X", "alumno6");
        Alumne a7 = new Alumne("72345678X", "alumno7");
        Alumne a8 = new Alumne("82345678X", "alumno8");
        Alumne a9 = new Alumne("92345678X", "alumno9");

        assignatura1.alumnes_matriculats.add(a1);
        assignatura1.alumnes_matriculats.add(a2);
        assignatura1.alumnes_matriculats.add(a3);
        assignatura2.alumnes_matriculats.add(a4);
        assignatura2.alumnes_matriculats.add(a5);
        assignatura2.alumnes_matriculats.add(a6);
        assignatura3.alumnes_matriculats.add(a7);
        assignatura3.alumnes_matriculats.add(a8);
        assignatura3.alumnes_matriculats.add(a9);

        assignaturaList.add(assignatura1.toDocument());
        assignaturaList.add(assignatura2.toDocument());
        assignaturaList.add(assignatura3.toDocument());

        collection.insertMany(assignaturaList);
    }

    public static void exercici2(MongoCollection collection){

        try (MongoCursor<Document> cursor
                     = collection.find(gt("hores_setmana", 2)).iterator()){
            while (cursor.hasNext()){
                Assignatura assignatura = new Assignatura(cursor.next());
                System.out.println(assignatura);
            }
        }


        /*
        List<Assignatura> clientsList = Assignatura.loadIterable(collection.find());
        for (Assignatura a : clientsList) {
            System.out.println(a.toString());
        }*/
    }


}
