package activitat3_1;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DatabaseMetaData;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.sql.Date;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="comanda")
public class Comanda implements Serializable {
	
        @Id
	protected int numComanda;
	
        @Column(name="preu_total")
        protected double preuTotal;
        
        @Column(name="data")
	protected Date data;


	public Comanda() {
		numComanda = 0;
		preuTotal = 0;
		data = null;
	}

	
	public Comanda(int numComanda, double preuTotal, Date data) {
		this.numComanda = numComanda;
		this.preuTotal = preuTotal;
		this.data = data;
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + numComanda;
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Comanda other = (Comanda) obj;
		if (numComanda != other.numComanda)
			return false;
		return true;
	}


	// Formatter
	public static SimpleDateFormat getFormatter() {
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy"); 
		return formatter;
	}
	
        public String dateToStr(Date date){
            Format formatter = new SimpleDateFormat("dd/MM/yyyy");
            String dataStr = formatter.format(date);
            return dataStr;
        }
        

	// Creació de taula comanda en mysql
	public static void createTable(Connection con) throws SQLException {
		if (!isTableExists(con)) {
			String sentenciaSQL = "CREATE TABLE comanda ("
					+ "num_comanda INT(5),"
					+ "preu_total DOUBLE(5,2),"
					+ "data DATE,"
					+ "dni_client INT(8),"
					+ "PRIMARY KEY (num_comanda),"
					+ "CONSTRAINT comanda_dni_client_fk FOREIGN KEY (dni_client) REFERENCES client(dni) ON DELETE CASCADE"
					+ ")";

			Statement statement = con.createStatement();
			statement.execute(sentenciaSQL);
			System.out.println("Taula comanda creada.");
		} else {
			System.err.println("ERROR: La taula comandes ja existeix!");
		}
	}
	
	
	// Comprovar si la taula existeix a la base de dades.
	public static boolean isTableExists(Connection con) {
		try {
			DatabaseMetaData dbm = con.getMetaData();
			ResultSet tables = dbm.getTables(null, null, "comanda", null);
			if (tables.next()) {
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	
		
	// Emmagatzemat de dades a la BBDD
	public void save(Connection con, int dni_client) {
		String sentenciaSQL = "insert into comanda (num_comanda, preu_total, data, dni_client) values(?,?,?,?)";
		try {
			PreparedStatement sentenciaPreparada = con.prepareStatement(sentenciaSQL);
			sentenciaPreparada.setInt(1, numComanda);
			sentenciaPreparada.setDouble(2, preuTotal);
			sentenciaPreparada.setDate(3, (java.sql.Date) data);
			sentenciaPreparada.setInt(4, dni_client);
			sentenciaPreparada.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	
	
	// Alta d'una nova comanda
	public static void newComanda(HashSet<Client> clients) {

		// Demanar dades del client
		String nomClient = Client.printDniNom(clients);

		if (nomClient != "Client no existeix!") {

			// Demanar dades de la Comanda
			System.out.print("Introdueix el número de comanda: ");
			int num = Client.getScanner().nextInt();
			System.out.print("Introdueix el Preu: ");
			Double preu = Client.getScanner().nextDouble();
			System.out.print("Introdueix la Data (dd/MM/yyyy): ");
			String dataString = Client.getScanner().next();
                        
			Date data = null;
            try {
            	//Convert to java.util.Date to java.sql.Date
            	java.util.Date utilDate = getFormatter().parse(dataString);
                data = new Date(utilDate.getTime());
            } catch (ParseException ex) {
                Logger.getLogger(Comanda.class.getName()).log(Level.SEVERE, null, ex);
            }

			// Crear objecte Comanda temporal
			Comanda comandaTmp = new Comanda(num, preu, data);
			
			for (Client client : clients) {
				
				if (client.comandes.contains(comandaTmp)) {
					System.err.println("La comanda amb numComanda "+comandaTmp.numComanda+" ja existeix a la taula!");
					break;
				}
				else if (nomClient == client.nom) {
					client.comandes.add(comandaTmp);					
					comandaTmp.save(ConnDB.startConn(), client.dni);
				}
			}
			
		} else {
			System.out.println(nomClient);
		}
	}
        
        
        
	// Imprimir dades
	public void print() {
		StringBuilder sb = new StringBuilder();
		sb.append("\tNumComanda: ").append(numComanda).append(System.lineSeparator());
		sb.append("\tPreuTotal: ").append(preuTotal).append(System.lineSeparator());
		sb.append("\tData: ").append(dateToStr(data)).append(System.lineSeparator());
		System.out.println(sb.toString());
	}
}
