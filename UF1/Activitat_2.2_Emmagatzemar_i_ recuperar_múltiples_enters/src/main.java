import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

public class main {

	public static String getRuta() {
		String ruta = "/home/users/inf/wiam2/iam2318925/TestM06/file1";
		return ruta;
	}

	public static Scanner getScanner() {
		Scanner sc = new Scanner(System.in);
		return sc;
	}

	public static void WriteInt() {

		System.out.println("Cuántos enteros quieres introducir?");

		int length = getScanner().nextInt();
		int[] nums = new int[length];

		// Guardar numeros en array
		for (int i = 0; i < nums.length; i++) {
			System.out.println("Introduzca un numero: ");
			nums[i] = getScanner().nextInt();
		}

		try {
			DataOutputStream streamWriter = new DataOutputStream(
					new BufferedOutputStream(new FileOutputStream(getRuta())));

			// En el archivo guardo la longitud de array en la primera posición.
			streamWriter.writeInt(nums.length);

			// Guardar numeros en archivo
			for (int i = 0; i < nums.length; i++) {
				streamWriter.writeInt(nums[i]);
			}
			streamWriter.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static void ReadInt() {

		try {
			DataInputStream streamReader = new DataInputStream(
					new BufferedInputStream(new FileInputStream(getRuta())));

			int length = streamReader.readInt();
			int[] nums = new int[length];

			for (int i = 0; i < nums.length; i++) {
				nums[i] = streamReader.readInt();
			}
			streamReader.close();

			for (int i = 0; i < nums.length; i++) {
				System.out.println(nums[i]);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {

		System.out.print("Elige una opción: \n1.Escribir Enteros\n2.Leer Enteros\n:");
		int opcion = getScanner().nextInt();

		if (opcion == 1) {
			WriteInt();
		} else {
			ReadInt();
		}

	}

}
