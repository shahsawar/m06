import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Politic {

	/* Per a cadascun dels següents conceptes digues si pertanyen al món 
	 * de les BBDD o al de l'OO i si tenen alguna relació entre ells:
	 
	 * Taula - BBDD
	 * Atribut - BBDD i OO
	 * columna- BBDD
	 * classe - OO
	 * objecte - OO
	 * */
	
	protected String nif;
	protected String nom;
	protected LocalDate dataNaixement;
	protected int sou;
	protected boolean esCorrupte;
	
	
	public Politic() {
		
	}
	
	public Politic(String nif, String nom, LocalDate dataNaixement, int sou, boolean esCorrupte) {
		this.nif = nif;
		this.nom = nom;
		this.dataNaixement = dataNaixement;
		this.sou = sou;
		this.esCorrupte = esCorrupte;
	}
	
	public String boolToString() {
		if (esCorrupte) {
			return "Si";
		}
		return "No";
	}
	

	
	public void printData() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		System.out.println("Dades Polític:");
		StringBuilder sb = new StringBuilder();
		sb.append("    NIF: ").append(nif).append(System.lineSeparator());
		sb.append("    NOM: ").append(nom).append(System.lineSeparator());
		sb.append("    DataNaixament: ").append(dataNaixement.format(formatter)).append(System.lineSeparator());
		sb.append("    Sou: ").append(sou).append(System.lineSeparator());
		sb.append("    esCorrupte: ").append(boolToString()).append(System.lineSeparator());
		System.out.println(sb.toString());
	}
	
	
	
	// Programa un mètode que demani les 5 dades (nif, nom, dataNaixement, sou, esCorrupte)
	// i que retorni un objecte de la classe Politic.
	public static Politic readData() {
		
		// Scanner i el format de data
		Scanner sc = new Scanner(System.in);
        	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy"); 

		// Demanar dades a l'usuari
		System.out.print("Introdueix el NIF: ");
		String nif = sc.nextLine();
		System.out.print("Introdueix el Nom: ");
		String nom = sc.nextLine();
		System.out.print("Introdueix la Data de naixement: ex (03-03-2003): ");
		String dataNaixementString = sc.nextLine();
		System.out.print("Introdueix el Sou: ");
		int sou = sc.nextInt();
		System.out.print("És corrupte? (s/n): ");
		boolean esCorrupte = sc.next().equalsIgnoreCase("s");
		
	
		// Crear un objecte polític amb les dades introduïdes i tornar-lo.
		Politic politicTmp = new Politic();
		try {
			// Convertir data string a data
			LocalDate dataNaixement = LocalDate.parse(dataNaixementString,formatter);
			politicTmp = new Politic(nif, nom, dataNaixement, sou, esCorrupte);
		} catch (Exception e) {
			throw e;
		}

		return politicTmp;
	}
	
	
	
	//Programa també un mètode que, donat l'objecte de la classe Politic, insereixi el polític a la taula.
	public void save(Connection con) throws SQLException {
		String sentenciaSQL = "insert into politics (nif, nom, dataNaixament, sou, esCorrupte) values(?,?,?,?,?)";
		PreparedStatement sentenciaPreparada = con.prepareStatement(sentenciaSQL);
		sentenciaPreparada.setString(1, nif);
		sentenciaPreparada.setString(2, nom);
		sentenciaPreparada.setDate(3, java.sql.Date.valueOf(dataNaixement));
		sentenciaPreparada.setInt(4, sou);
		sentenciaPreparada.setBoolean(5, esCorrupte);
		sentenciaPreparada.executeUpdate();
	}
	
	
	
	// Programa un mètode que llegeixi de la taula totes les files i construeixi una llista d'objectes de la classe Politic a partir de les files que hi ha a la taula.
	public static List<Politic> readPoliticFromTable(Connection con) throws SQLException, ParseException {
	
		List<Politic> politics = new ArrayList<Politic>();
		
		String sentenciaSQL = "select * from politics";
		Statement statement = con.createStatement();
		ResultSet rs = statement.executeQuery(sentenciaSQL);
		
		while (rs.next()) {
			Politic politicTmp = new Politic(rs.getString("nif"), rs.getString("nom"),
					rs.getDate("dataNaixament").toLocalDate(), rs.getInt("sou"),
					rs.getBoolean("esCorrupte"));
			politics.add(politicTmp);
		}
		return politics;
	}
	
	
	
	// Creació de taula en mysql
	public static void createTable(Connection con) throws SQLException {
		String sentenciaSQL = "CREATE TABLE IF NOT EXISTS politics ("
				+ "nif VARCHAR(9) primary key,"
				+ "nom VARCHAR(50),"
				+ "dataNaixament DATE,"
				+ "sou int(10),"
				+ "esCorrupte CHAR(1)"
				+ ")";
		
		Statement statement = con.createStatement();
		statement.execute(sentenciaSQL);		
		System.out.println("Taula polítics creada");
	}
}
