package com.gitlab.shahsawar;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;


@Entity
@Table(name = "client")
public class Client implements Serializable {

    @Id
    private int dni;
    private String nom;
    private Boolean premium;

    // Scanner
    public static Scanner getScanner() {
        Scanner sc = new Scanner(System.in);
        return sc;
    }

    //@OneToMany
    //@JoinColumn(name = "dni_client")
    @OneToMany(mappedBy = "dni_client", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    Set<Comanda> comandes = new HashSet<>();

    public Client() {
    }

    public Client(int dni, String nom, Boolean premium) {
        this.dni = dni;
        this.nom = nom;
        this.premium = premium;
        comandes = new HashSet<>();
    }

    public int getDni() {
        return dni;
    }

    public void setDni(int dni) {
        this.dni = dni;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Boolean getPremium() {
        return premium;
    }

    public void setPremium(Boolean premium) {
        this.premium = premium;
    }

    public Set<Comanda> getComandes() {
        return comandes;
    }

    public void setComandes(Set<Comanda> comandes) {
        this.comandes = comandes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return dni == client.dni;
    }

    @Override
    public int hashCode() {
        return Objects.hash(dni);
    }

    // Return a list of all clients in database
    public static List<Client> readFromDataBase(SessionFactory factory) {
        try (Session session = factory.openSession()) {
            session.beginTransaction();
            return session.createQuery("from Client").getResultList();
        }
    }

    // Delete client from database
    public void delete(SessionFactory factory) {
        try (Session session = factory.openSession()) {
            session.beginTransaction();
            session.createQuery("delete from Comanda where dni_client= :dni").setParameter("dni", getDni()).executeUpdate();
            session.delete(this);
            session.getTransaction().commit();
        }
        System.out.println("Client Successfully Deleted.");
    }

    // Update client in database
    public void update(SessionFactory factory) {

        System.out.print("Enter the new name: ");
        String name = getScanner().next();

        System.out.print("Is it premium? (s/n): ");
        Boolean premium = getScanner().next().equalsIgnoreCase("S");

        try (Session session = factory.openSession()) {
            session.beginTransaction();
            //Client c = session.createQuery("from Client c where c.dni=:id", Client.class).setParameter("id", getDni()).getSingleResult();
            setNom(name);
            setPremium(premium);
            session.update(this);
            session.getTransaction().commit();
        }
        System.out.println("Client Successfully Updated.");
    }

    // Find clients by name in database
    public static void findClientByName(SessionFactory factory) {
        System.out.print("Find client whose name begins with: ");
        String letters = getScanner().next();

        try (Session session = factory.openSession()) {
            session.beginTransaction();
            List<Client> cli = session.createQuery("from Client where nom LIKE Concat('" + letters + "','%')").getResultList();
            Iterator it = cli.iterator();
            while (it.hasNext()) {
                Client emp = (Client) it.next();
                System.out.println(emp.getNom());
            }
        }
    }

    // Add client in database
    public static void add(SessionFactory factory, List<Client> clients) {

        // Read client info
        System.out.print("Enter a dni: ");
        int dni = getScanner().nextInt();
        System.out.print("Enter a name: ");
        String name = getScanner().next();
        System.out.print("Is it premium? (s/n): ");
        Boolean premium = getScanner().next().equalsIgnoreCase("S");

        Client clientTmp = new Client(dni, name, premium);

        if (!clients.contains(clientTmp)) {
            try (Session session = factory.openSession()) {
                session.beginTransaction();
                session.save(clientTmp);
                session.getTransaction().commit();
            } catch (Exception e) {
                e.getStackTrace();
            }
        } else {
            System.err.println("The client already exists!");
        }
    }

    // Show client order history
    public static void showClientComandes(List<Client> clients) {
        Client client = returnClientSelected(clients);
        System.out.println("Nom: " + client.getNom() + "\n" + "Dni: " + client.getDni() + " :\n");
        for (Comanda c : client.comandes) {
            System.out.println("\tNum Comanda: " + c.getNum_comanda());
            System.out.println("\tPreu Total: " + c.getPreu_total());
            System.out.println("\tData: " + c.getData() + "\n");
        }
    }

    // Return client selected
    public static Client returnClientSelected(List<Client> clients) {

        //Show all clients
        int cont = 1;
        for (Client c : clients) {
            System.out.println(cont + ") " + c.getDni() + "\t" + c.getNom());
            cont++;
        }
        System.out.println();

        System.out.print("Select the client: ");
        int optionSelected = getScanner().nextInt();

        if ((optionSelected != 0) && (optionSelected < cont)) {
            //Return client selected
            cont = 1;
            for (Client c : clients) {
                if (optionSelected == cont) {
                    return c;
                }
                cont++;
            }
        } else {
            System.err.println("This option does not exist!");
        }
        return new Client();
    }

    // Add client with his order list
    public static void saveClient(SessionFactory factory, Client client) {
        try (Session session = factory.openSession()) {
            session.beginTransaction();
            session.save(client);
            for (Comanda c : client.comandes) {
                session.save(c);
            }
            session.getTransaction().commit();
        }
    }

    // Return billing summary
    public void showResumFacturacio(SessionFactory sessionFactory){
        try(Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            List<Double> preuTotal = session.createQuery("select sum(preu_total) from Comanda where dni_client = :dni").setParameter("dni", getDni()).getResultList();
            for (Double preu : preuTotal ){
                if (preu != null){
                    System.out.println("Dni: "+getDni()+"\tPreuTotal: "+preu);
                }
            }
            /*
            List<Double> listResum = session.createQuery("select sum(quantitat) from ResumFacturacio where dni_client= :dni").setParameter("dni_client",11111111).getResultList();
            Iterator it = listResum.iterator();
            while (it.hasNext()) {
                Double emp = (Double) it.next();
                System.out.println(emp.toString());
            }
            session.getTransaction().commit();*/
        }
    }

}
