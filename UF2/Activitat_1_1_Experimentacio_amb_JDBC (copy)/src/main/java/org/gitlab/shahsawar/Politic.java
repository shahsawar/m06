package org.gitlab.shahsawar;

import com.mysql.cj.xdevapi.Client;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.*;
import java.util.List;
import java.util.Scanner;

@Entity
@Table(name = "politic")
public class Politic implements Serializable {

    @Id
    private String nif;
    private String nom;
    private Date dataNaixement;
    private int sou;
    private Boolean esCorrupte;

    public Politic() {
    }

    public Politic(String nif, String nom, Date dataNaixement, int sou, Boolean esCorrupte) {
        this.nif = nif;
        this.nom = nom;
        this.dataNaixement = dataNaixement;
        this.sou = sou;
        this.esCorrupte = esCorrupte;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Date getDataNaixement() {
        return dataNaixement;
    }

    public void setDataNaixement(Date dataNaixement) {
        this.dataNaixement = dataNaixement;
    }

    public int getSou() {
        return sou;
    }

    public void setSou(int sou) {
        this.sou = sou;
    }

    public Boolean getEsCorrupte() {
        return esCorrupte;
    }

    public void setEsCorrupte(Boolean esCorrupte) {
        this.esCorrupte = esCorrupte;
    }


    // Create table politic
    public static void createTable(Connection con) {
        if (!isTableExists(con)) {
            try(Statement statement = con.createStatement()){
                String sentenciaSQL = "CREATE TABLE politic ("
                        + "nif varchar(9),"
                        + "nom varchar(30),"
                        + "dataNaixement DATE,"
                        + "sou INT(10),"
                        + "esCorrupte boolean,"
                        + "PRIMARY KEY (nif)"
                        + ")";
                statement.execute(sentenciaSQL);
                System.out.println("Politic table successfully created.");
            }
            catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        } else {
            System.err.println("ERROR: The politic table already exists!");
        }
    }

    // Check if the table politic exists in the database
    public static boolean isTableExists(Connection con) {
        try {
            DatabaseMetaData dbm = con.getMetaData();
            ResultSet tables = dbm.getTables(null, null, "politic", null);
            if (tables.next()) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    // Save politic into db.
    public void add(SessionFactory sessionFactory){
        try(Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(this);
            session.getTransaction().commit();
            System.out.println("Politic has been successfully added to the table.");
        }catch (Exception e){
            System.err.println("The politic is already exist in database!");
            e.printStackTrace();
        }
    }

    // Create and return politic
    public static Politic read(){
        // Read politic info
        System.out.print("Enter nif: ");
        String nif = PoliticMain.getScanner().next();
        System.out.print("Enter name: ");
        String name = PoliticMain.getScanner().next();
        System.out.print("Enter date of birth (format: yyyy-MM-dd): ");
        Date dateB = PoliticMain.setDate(PoliticMain.getScanner().next());
        System.out.print("Enter salary: ");
        int salary = PoliticMain.getScanner().nextInt();
        System.out.print("Is it corrupt? (s/n): ");
        Boolean corrupt = PoliticMain.getScanner().next().equalsIgnoreCase("S");

        return new Politic(nif, name, dateB, salary, corrupt);
    }


    // Returns a list of all politic that are DB.
    public static List<Politic> readFromDB(SessionFactory sessionFactory){
        try(Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            return session.createQuery("From Politic").getResultList();
        }
    }

    // Show politic data
    public static void print(Politic politic){
        System.out.println(politic);
    }


    @Override
    public String toString() {
        return "Politic{" +
                "nif='" + nif + '\'' +
                ", nom='" + nom + '\'' +
                ", dataNaixement=" + dataNaixement +
                ", sou=" + sou +
                ", esCorrupte=" + esCorrupte +
                '}';
    }
}
