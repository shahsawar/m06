package activitat1_2;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Scanner;

public class Main {
	
	public static DateTimeFormatter getFormatter() {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		return formatter;
	}
	
	public static int menu() {
		Scanner sc = new Scanner(System.in);
		System.out.print("\n"
				+ "0. Sortir\n"
				+ "1. Creació del model\n"
				+ "2. Recuperació de les dades de la BBDD\n"
				+ "3. Emmagatzemat de dades a la BBDD\n"
				+ "4. Alta d'un nou client\n"
				+ "5. Alta d'una nova comanda\n"
				+ "6. Mostrar per pantalla les comandes d'un client\n\n: ");
		return sc.nextInt();
	}
	
	
	public static void main(String[] args) throws SQLException {
	
		HashSet<Client> clients = new HashSet<Client>();
		
		
		Client client1 = new Client(12345671, "ABC", false);
		Client client2 = new Client(12345672, "ABD", true);
		Client client3 = new Client(12345673, "ABE", true);
		Client client4 = new Client(12345674, "ABG", false);
		Client client5 = new Client(12345671, "ABI", true);
		
		clients.add(client1);
		clients.add(client2);
		clients.add(client3);
		clients.add(client4);
		clients.add(client5);
		
		Comanda comanda1 = new Comanda(1, 10.00, LocalDate.parse("10-10-2010", getFormatter()));
		Comanda comanda2 = new Comanda(2, 20.00, LocalDate.parse("14-01-2010", getFormatter()));
		Comanda comanda3 = new Comanda(3, 30.00, LocalDate.parse("12-02-2010", getFormatter()));
		Comanda comanda4 = new Comanda(4, 40.00, LocalDate.parse("11-04-2010", getFormatter()));
		
		client1.comandes.add(comanda1);
		client1.comandes.add(comanda2);
		client2.comandes.add(comanda3);
		client4.comandes.add(comanda4);
		
		
		int op = menu();

		while (op != 0) {
			switch (op) {
			case 0:
				op = 0;
				break;
			case 1:
				Client.createTable(ConnDB.startConn());
				ConnDB.closeConn();
				Comanda.createTable(ConnDB.startConn());
				ConnDB.closeConn();
				break;
				
			case 2:
				clients = Client.read(ConnDB.startConn());
				ConnDB.closeConn();
				break;
				
			case 3:
				// Elimina les dades de la taules
				Client.deleteTablesData(ConnDB.startConn(), "client");
				Client.deleteTablesData(ConnDB.startConn(), "comanda");
				
				// Guardar tots els clients amb les seves comandes
				for (Client client : clients) {
					client.save(ConnDB.startConn());
					for (Comanda comanda : client.comandes) {
						comanda.save(ConnDB.startConn(), client.dni);
					}
				}
				ConnDB.closeConn();
				break;
				
			case 4:
				Client clientTmp = Client.newClient();
				if (clients.contains(clientTmp)) {
					System.err.println("El client amb dni "+clientTmp.dni+" ja existeix a la taula!");
				} else {
					clients.add(clientTmp);
				}
				break;
				
			case 5:
				Comanda.newComanda(clients);
				break;
				
			case 6:
				Client.showComandes(clients);
				break;
			default:
				System.err.println("Aquesta opció no és vàlida");
				break;
			}
			op = menu();
		}
		System.out.println("Surt del programa.");
	}
}
