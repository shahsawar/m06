CREATE TABLE client (
    dni INT(8),
    nom VARCHAR(20),
    premium ENUM('S', 'N'),
    PRIMARY KEY (dni)
);

insert into client(dni, nom, premium) values(12344666, 'lolito', 'N');



CREATE TABLE comanda (
    num_comanda INT(5),
    preu_total DOUBLE(5 , 2 ),
    data DATE,
    dni_client INT(8),
    PRIMARY KEY (num_comanda),
    CONSTRAINT comanda_dni_client_fk FOREIGN KEY (dni_client)
        REFERENCES client (dni)
);

insert into comanda (num_comanda, preu_total, data, dni_client) values(6,99.99,str_to_date('27-01-2020', '%d-%m-%Y'),12345688);



-- Table resum_facturacio Creation.
CREATE TABLE IF NOT EXISTS resum_facturacio (
    mes INT,
    any INT,
    dni_client INT,
    quantitat NUMERIC(10 , 2 ),
    CONSTRAINT PK_RESUM PRIMARY KEY (mes , any, dni_client),
    FOREIGN KEY (dni_client)
        REFERENCES client (dni)
);
