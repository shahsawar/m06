use lamevabbdd;

-- Table resum_facturacio Creation.
CREATE TABLE IF NOT EXISTS resum_facturacio (
    mes INT,
    any INT,
    dni_client INT,
    quantitat NUMERIC(10 , 2 ),
    CONSTRAINT PK_RESUM PRIMARY KEY (mes , any , dni_client),
    FOREIGN KEY (dni_client)
        REFERENCES client (dni)
);


DROP PROCEDURE IF EXISTS crea_resum_facturacio;
DELIMITER //
CREATE PROCEDURE crea_resum_facturacio(IN p_mes varchar(2), p_any varchar(4)) BEGIN
DECLARE v_dni_client INT;
DECLARE v_preu_total numeric(10,2);
DECLARE acaba INT DEFAULT FALSE; 

DECLARE c_comanda CURSOR FOR SELECT 
    dni_client, SUM(preu_total)
FROM
    comanda
WHERE
    MONTH(data) = p_mes AND YEAR(data) = p_any
GROUP BY dni_client;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET acaba = true;
OPEN c_comanda;
read_loop: LOOP
	FETCH c_comanda INTO v_dni_client, v_preu_total; 
    IF acaba THEN
        LEAVE read_loop;
    END IF;
	INSERT INTO resum_facturacio (mes, any, dni_client, quantitat) VALUES (p_mes, p_any, v_dni_client, v_preu_total);
END LOOP;
CLOSE c_comanda; 
END;
//

call crea_resum_facturacio(01, 2020);

/* 
select sum(preu_total) as total, dni from client INNER JOIN comanda on comanda.dni_client = client.dni group by dni_client;
select sum(preu_total) as total, dni from client INNER JOIN comanda on comanda.dni_client = client.dni group by dni_client;

SELECT 
    dni_client, SUM(preu_total)
FROM
    comanda
WHERE
    MONTH(data) = 1 AND YEAR(data) = 2010
GROUP BY dni_client
*/