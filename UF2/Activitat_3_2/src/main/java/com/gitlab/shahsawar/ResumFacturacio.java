package com.gitlab.shahsawar;

import org.hibernate.SessionFactory;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "resum_facturacio")
public class ResumFacturacio {

    private ResumFacturacioPk resumFacturacioPk;
    private Double quantitat;


    public ResumFacturacio() {
    }

    public ResumFacturacio(ResumFacturacioPk resumFacturacioPk, Double quantitat) {
        this.resumFacturacioPk = resumFacturacioPk;
        this.quantitat = quantitat;
    }

    @Id
    public ResumFacturacioPk getResumFacturacioPk() {
        return resumFacturacioPk;
    }

    public void setResumFacturacioPk(ResumFacturacioPk resumFacturacioPk) {
        this.resumFacturacioPk = resumFacturacioPk;
    }

    public Double getQuantitat() {
        return quantitat;
    }

    public void setQuantitat(Double quantitat) {
        this.quantitat = quantitat;
    }
}
