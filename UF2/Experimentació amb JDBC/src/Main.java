import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
	
	
	public static void main(String[] args) throws ClassNotFoundException, SQLException, ParseException {
		
		List<Politic> listPolitics;
		
		// Dades de connexió de base de dades.
		String url="jdbc:mysql://localhost/lamevabbdd";
		String usuari="lamevaapp";
		String password="lamevaapp";
		
		
		// Iniciar la connexió a la base de dades
		Connection con = (Connection) DriverManager.getConnection(url, usuari, password);
		//Politic.createTable(con);
		Politic politic = Politic.readData();
		politic.save(con);
		listPolitics = Politic.readPoliticFromTable(con);
		con.close();		
		
		
		// Mostrant les dades de l'polítics
		for (Politic politico : listPolitics) {
			politico.printData();
		}
		
	}
	
	
}
