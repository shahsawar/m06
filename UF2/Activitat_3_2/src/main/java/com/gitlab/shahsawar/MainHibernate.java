/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gitlab.shahsawar;

import java.sql.Date;
import java.time.LocalDate;
import java.util.*;
import java.util.logging.Level;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

/**
 * @author shah
 */
public class MainHibernate {

    // Return date in sql format
    public static Date setDate(String strDate) {
        LocalDate date = LocalDate.parse(strDate);
        return Date.valueOf(date);
    }

    // Read all clients from database and save in list.
    public static void updateClientListFromDB(SessionFactory sessionFactory, List<Client> clients) {
        clients.clear();
        clients.addAll(Client.readFromDataBase(sessionFactory));
    }

    // Menu
    public static int menu() {
        Scanner sc = new Scanner(System.in);
        System.out.print("\n"
                + "0. Sortir\n"
                + "1. Eliminació d'un client\n"
                + "2. Actualització de les dades d'un client\n"
                + "3. Mostra per pantalla els clients que el seu nom comenci per un text introduït\n"
                + "4. Alta d'un nou client\n"
                + "5. Alta d'una nova comanda\n"
                + "6. Mostrar per pantalla les comandes d'un client\n"
                + "7. Generació de resum de facturació\n\n: ");
        return sc.nextInt();
    }


    public static void main(String[] args) {

        //Show only warning info
        java.util.logging.Logger.getLogger("org.hibernate").setLevel(Level.WARNING);

        //List Client
        List<Client> clients = new ArrayList<>();

        // Creating client objects
        Client client1 = new Client(12341334, "TestHibernate1", true);
        Client client2 = new Client(12341335, "TestHibernate2", false);
        Client client3 = new Client(12341336, "TestHibernate3", true);

        // Creating comanda objects
        Comanda comanda1 = new Comanda(12345, 19.00, setDate("2020-03-19"), client1.getDni());
        Comanda comanda2 = new Comanda(12346, 29.00, setDate("2020-03-19"), client1.getDni());
        Comanda comanda3 = new Comanda(12347, 19.00, setDate("2020-03-19"), client2.getDni());
        Comanda comanda4 = new Comanda(12348, 29.00, setDate("2020-03-19"), client3.getDni());
        Comanda comanda5 = new Comanda(12349, 19.00, setDate("2020-03-19"), client2.getDni());
        Comanda comanda6 = new Comanda(12350, 29.00, setDate("2020-03-19"), client2.getDni());
        Comanda comanda7 = new Comanda(12351, 19.00, setDate("2020-03-19"), client3.getDni());
        Comanda comanda8 = new Comanda(12352, 29.00, setDate("2020-03-19"), client2.getDni());

        // Adding orders to clients
        client1.comandes.add(comanda1);
        client1.comandes.add(comanda2);
        client2.comandes.add(comanda3);
        client2.comandes.add(comanda5);
        client2.comandes.add(comanda6);
        client2.comandes.add(comanda8);
        client3.comandes.add(comanda4);
        client3.comandes.add(comanda7);

        // Create session with database
        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().configure().build();
        SessionFactory sessionFactory = new MetadataSources(serviceRegistry).buildMetadata().buildSessionFactory();

        updateClientListFromDB(sessionFactory, clients);


        // Displaying a looping menu on the screen.
        int exit = menu();
        while (exit != 0) {
            switch (exit) {
                case 1:
                    Client c1 = Client.returnClientSelected(clients);
                    c1.delete(sessionFactory);
                    updateClientListFromDB(sessionFactory, clients);
                    break;
                case 2:
                    Client c2 = Client.returnClientSelected(clients);
                    c2.update(sessionFactory);
                    updateClientListFromDB(sessionFactory, clients);
                    break;
                case 3:
                    Client.findClientByName(sessionFactory);
                    break;
                case 4:
                    Client.add(sessionFactory, clients);
                    updateClientListFromDB(sessionFactory, clients);
                    break;
                case 5:
                    Client c3 = Client.returnClientSelected(clients);
                    Comanda.add(sessionFactory, c3);
                    updateClientListFromDB(sessionFactory, clients);
                    break;
                case 6:
                    Client.showClientComandes(clients);
                    break;
                case 7:
                    for (Client c : clients) {
                        c.showResumFacturacio(sessionFactory);
                    }
                    break;
                default:
                    System.err.println("This option does not exist!");
                    break;
            }
            exit = menu();
        }





        // Close all connections.
        sessionFactory.close();
        StandardServiceRegistryBuilder.destroy(serviceRegistry);
    }



    /*
    private static SessionFactory sessionFactory;

    private static SessionFactory buildSessionFactory(){
        //Create session with database
        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().configure().build();
        SessionFactory sessionFactory = new MetadataSources(serviceRegistry).buildMetadata().buildSessionFactory();
        return sessionFactory;
    }

    public static SessionFactory getSessionFactory() {
        if(sessionFactory == null) sessionFactory = buildSessionFactory();
        return sessionFactory;
    }*/


}