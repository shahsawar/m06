package activitat1_2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnDB {

	static Connection con;
	
	public static Connection startConn(){
		
		// Dades de connexió de base de dades.
		String url="jdbc:mysql://localhost/lamevabbdd";
		String usuari="lamevaapp";
		String password="lamevaapp";
		
		// Iniciar la connexió a la base de dades
		try {
			con = (Connection) DriverManager.getConnection(url, usuari, password);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return con;
	}
	
	// Tancar la connexió a la base de dades.
	public static void closeConn() {
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
