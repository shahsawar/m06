package m03_Activitat_3_1_ShahSawar;

import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathExpressionException;
import org.xml.sax.SAXException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jsola
 */
public class Ac3_1 {
    
    public static void escriure() throws ParserConfigurationException, TransformerException {
        Llibreria casa = new Llibreria("Casa del llibre", "Vic");
        Llibre llib1 = new Llibre("El quadern gris","Josep Pla");
        Llibre llib2 = new Llibre("El perquè de tot plegat","Quim Monzó");
        casa.llibres.add(llib1);
        casa.llibres.add(llib2);
        casa.desar_xml("llibreria.xml");
    }
    
    public static void llegir_seq() throws ParserConfigurationException, SAXException, IOException {
        Llibreria casa = new Llibreria();
        casa.llegir_sequencial("llibreria.xml");
        casa.imprimir();
    }
    
    public static void llegir_sin() throws ParserConfigurationException, SAXException, IOException, XPathExpressionException {
        Llibreria casa = new Llibreria();
        casa.llegir_sintactic("llibreria.xml");
        casa.imprimir();
    }

    public static void main(String[] args) throws ParserConfigurationException, TransformerException, SAXException, IOException, XPathExpressionException {  
        
    	llegir_sin();
        
    }
    
}
