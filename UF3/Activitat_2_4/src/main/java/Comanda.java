import org.bson.Document;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Comanda {

    // Attributes
    private Date dataComanda;
    private int importe;
    private Boolean pagada;

    // Constructor
    public Comanda(Date dataComanda, int importe, Boolean pagada) {
        this.dataComanda = dataComanda;
        this.importe = importe;
        this.pagada = pagada;
    }

    // Convert document to comanda
    public Comanda(Document doc) {
        this.dataComanda = doc.getDate("data_comanda");
        this.importe = doc.getInteger("importe");
        this.pagada = doc.getBoolean("pagada");
    }

    // Convert comanda to document
    public Document toDoc() {
        Document docTmp = new Document();
        docTmp.append("data_comanda", dataComanda);
        docTmp.append("importe", importe);
        docTmp.append("pagada", pagada);
        return docTmp;
    }

    public Date getDataComanda() {
        return dataComanda;
    }

    public void setDataComanda(Date dataComanda) {
        this.dataComanda = dataComanda;
    }

    public int getImporte() {
        return importe;
    }

    public void setImporte(int importe) {
        this.importe = importe;
    }

    public Boolean getPagada() {
        return pagada;
    }

    public void setPagada(Boolean pagada) {
        this.pagada = pagada;
    }

    public String getDate(){
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        return dateFormat.format(dataComanda);
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("\tData Comanda: ").append(getDate()).append(System.lineSeparator());
        sb.append("\tImporte: ").append(importe).append(System.lineSeparator());
        sb.append("\tPagada: ").append(pagada ? "Si":"No").append(System.lineSeparator());
        return sb.toString();
    }
}
