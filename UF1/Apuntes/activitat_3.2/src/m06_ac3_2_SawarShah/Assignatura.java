package m06_ac3_2_SawarShah;

import java.util.ArrayList;
import java.util.List;

import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Assignatura {

	// Atributs
	public int numero;
	public String nom;
	public int durada;
	public List<Alumne> alumnes = new ArrayList<>();

	
	// Constructor buit
	public Assignatura() {}
	
	
	// Constructor con parámetros
	public Assignatura(int numero, String nom, int durada) {
		this.numero = numero;
		this.nom = nom;
		this.durada = durada;
	}

	
	// Imprimir dades per pantalla
	public void imprimir() {
		
		System.out.println("Assignatura: ");
		System.out.println("    Número: "+ numero);
		System.out.println("    Nom: "+ nom);
		System.out.println("    Durada: "+ durada);
		System.out.println("    Alumnes:");
		for (Alumne alumne : alumnes) {
			System.out.println(
					"\tNom: "+alumne.nom+"\n"+
					"\tDni: "+alumne.dni+"\n"+
					"\tRepetidor: "+alumne.esRepetidor()
					);
			System.out.println("\t---------------");
		}
		System.out.println();
	}
	
	
	// Guardar en disc les assignatures
	public void desar_xml(Element nodeAssignaturas, Document doc) {
		
		// Tag assignatura
		Element nodeAssignatura = doc.createElement("assignatura");

		// Tag numero
		Element nodeNumero = doc.createElement("numero");
		nodeNumero.setTextContent(Integer.toString(numero));
		nodeAssignatura.appendChild(nodeNumero);

		// Tag nom
		Element nodeNom = doc.createElement("nom");
		nodeNom.setTextContent(nom);
		nodeAssignatura.appendChild(nodeNom);

		// Tag durada
		Element nodeDurada = doc.createElement("durada");
		nodeDurada.setTextContent(Integer.toString(durada));
		nodeAssignatura.appendChild(nodeDurada);

		// Tag alumnes
		Element nodeAlumnos = doc.createElement("alumnes");
		nodeAssignatura.appendChild(nodeAlumnos);

		// Per cada alumne
		for (Alumne alumne : alumnes) {
			alumne.desar_xml(nodeAlumnos, doc);
		}
		
		//Guardar el tag nodeAssignatura amb la seva informació a tag node Assignatures.
		nodeAssignaturas.appendChild(nodeAssignatura);
	}
	
	
	
	// Llegir les assignatures del disc amb mètode seqüencial.
	public void llegirXmlSeq(NodeList nodelist) {
		
		for (int i = 0; i < nodelist.getLength(); i++) {
			
			if (nodelist.item(i).getNodeName().equals("numero")) {
				this.numero = Integer.parseInt(nodelist.item(i).getTextContent().trim()); 
			}
			if (nodelist.item(i).getNodeName().equals("nom")) {
				this.nom = nodelist.item(i).getTextContent().trim();
			}
			if (nodelist.item(i).getNodeName().equals("durada")) {
				this.durada = Integer.parseInt(nodelist.item(i).getTextContent().trim()); 
			}
			
			if (nodelist.item(i).getNodeName().equals("alumnes")) {
				NodeList alumnes = nodelist.item(i).getChildNodes();
				
				for (int j = 0; j < alumnes.getLength(); j++) {
					Alumne alumne = new Alumne();
					alumne.llegir_xml(alumnes.item(j));
					this.alumnes.add(alumne);
				}
			}
		}	
	}
	
	
	// Llegir les assignatures del disc amb mètode sintàctics.
	public void llegirXmlSin(Document doc, XPathExpression expr,int i) {
		
		try {
			expr = XPathFactory.newInstance().newXPath().compile("/assignaturas/assignatura["+(i+1)+"]/numero");
			Node node = (Node) expr.evaluate(doc, XPathConstants.NODE);
			this.numero = Integer.parseInt(node.getTextContent());
			
			expr = XPathFactory.newInstance().newXPath().compile("/assignaturas/assignatura["+(i+1)+"]/nom");
			node = (Node) expr.evaluate(doc, XPathConstants.NODE);
			this.nom = node.getTextContent();
			
			expr = XPathFactory.newInstance().newXPath().compile("/assignaturas/assignatura["+(i+1)+"]/durada");
			node = (Node) expr.evaluate(doc, XPathConstants.NODE);
			this.durada = Integer.parseInt(node.getTextContent());
			
			expr = XPathFactory.newInstance().newXPath().compile("/assignaturas/assignatura["+(i+1)+"]/alumnes");
			NodeList nodeList = (NodeList) expr.evaluate(doc, XPathConstants.NODE);
			
			for (int j = 0; j < nodeList.getLength(); j++) {
				expr = XPathFactory.newInstance().newXPath().compile("/assignaturas/assignatura["+(i+1)+"]/alumnes/alumne["+(j+1)+"]");
	            node = (Node) expr.evaluate(doc, XPathConstants.NODE);
	            Alumne alumne = new Alumne();
	            alumne.llegir_xml(node);
	            alumnes.add(alumne);
			}		
		} catch (Exception e) {}
	}	
}