/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hibernatetest;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import javax.persistence.Entity;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

/**
 *
 * @author shah
 */
public class Hibernate {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        java.util.logging.Logger.getLogger("org.hibernate").setLevel(Level.WARNING);
        
        Person p = new Person();
        p.dni = "12345678H";
        p.nom = "Patata";
        p.edat = 19;

        Configuration configuration = new Configuration();
        configuration.configure();
        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
        SessionFactory factory = configuration.buildSessionFactory(serviceRegistry);

        //savePerson(factory, p);
        List<Person> persones = readPersons(factory);
        
        for(Person pp : persones){
            System.out.println(pp);
        }
        
        
        factory.close();
        StandardServiceRegistryBuilder.destroy(serviceRegistry);
    }

    public static void savePerson(SessionFactory factory, Person p) {
        Session session = factory.openSession();

        try {
            session.beginTransaction();
            session.save(p);
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public static List<Person> readPersons(SessionFactory factory) {
        Session session = factory.openSession();
        List<Person> lstPersones = new ArrayList<>();

        try {
            Query query = session.createQuery("from Person");
            // from + el nom de la classe !!!
            lstPersones = query.list();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            session.close();
        }

        return lstPersones;
    }
 
}
