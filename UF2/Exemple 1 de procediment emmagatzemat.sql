Use lamevabbdd;
Drop table if exists persona;
CREATE TABLE persona (
    dni VARCHAR(10) PRIMARY KEY,
    data_naixement DATE
);

Drop procedure if exists insereix_persona;
Delimiter //
CREATE PROCEDURE insereix_persona (IN p_dni VARCHAR(10), p_data_naixement DATE)
BEGIN
insert into persona(dni, data_naixement) values (p_dni, p_data_naixement);
END //
DELIMITER ;

call insereix_persona('12345678Z', str_to_date('10-10-2000', '%d-%m-%Y'));
call insereix_persona('12345678X', str_to_date('10-10-2001', '%d-%m-%Y'));