package m03_Activitat_3_1_ShahSawar;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Jordi
 */
public class Llibreria {
    String nom;
    String poblacio;
    List<Llibre> llibres = new ArrayList<>();
    
    public Llibreria() {
        
    }

    public Llibreria(String nom, String poblacio) {
        this.nom = nom;
        this.poblacio = poblacio;
    }
    
    public void imprimir() {
        System.out.println("===== DADES DE LA LLIBRERIA ====");
        System.out.println("Nom: "+this.nom);
        System.out.println("Població: "+this.poblacio);
        System.out.println("Llibres:");
        for (Llibre llibre : this.llibres) {
            llibre.imprimir();
        }
        System.out.println("================================");
    }
    
    public void desar_xml(String ruta) throws ParserConfigurationException, TransformerConfigurationException, TransformerException {
        Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
        // tag llibreria
        Element nodeLlibreria = doc.createElement("llibreria");
        doc.appendChild(nodeLlibreria);
        // tag població
        Element nodePoblacio= doc.createElement("poblacio");
        nodePoblacio.setTextContent(this.poblacio);
        nodeLlibreria.appendChild(nodePoblacio);
        // tag nom
        Element nodeNom = doc.createElement("nom");
        nodeNom.setTextContent(this.nom);
        nodeLlibreria.appendChild(nodeNom);
        // desem els llibres
        Element nodeLlibres = doc.createElement("llibres");
        nodeLlibreria.appendChild(nodeLlibres);
        for (Llibre llibre : llibres) {
            llibre.desar_xml(nodeLlibres, doc);
        }
        // fem el desat a disc
        TransformerFactory.newInstance().newTransformer().
         transform(new DOMSource(doc), new StreamResult(new File (ruta)));
    }
    
    public void llegir_sequencial(String ruta) throws ParserConfigurationException, SAXException, IOException {
        Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(ruta);
        Node arrel = doc.getDocumentElement();
        NodeList llistaNodes = arrel.getChildNodes();
        
        
        // recorrem els nodes de llibreria
        for (int i=0; i<llistaNodes.getLength(); i++) {
            // si és nom
            if (llistaNodes.item(i).getNodeName().equals("nom"))
                // ho desem a l'atribut corresponent
                this.nom = llistaNodes.item(i).getTextContent();
            else if (llistaNodes.item(i).getNodeName().equals("poblacio"))
                this.poblacio = llistaNodes.item(i).getTextContent();
            // si és llibres
            
            else if (llistaNodes.item(i).getNodeName().equals("llibres")) {
                // obtenim els nodes fills
                NodeList llistaLlibres = llistaNodes.item(i).getChildNodes();
                // per cada node fill
                for (int j=0; j<llistaLlibres.getLength(); j++) {
                    // creem un llibre, el llegim i l'afegim
                    Llibre llibre = new Llibre();
                    llibre.llegir(llistaLlibres.item(j));
                    llibres.add(llibre);
                }                
            }
        }
    }
    
    public void llegir_sintactic(String ruta) throws XPathExpressionException, ParserConfigurationException, SAXException, IOException {
        Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(ruta);
        
        XPathExpression expr = XPathFactory.newInstance().newXPath().compile("/llibreria/nom");
        Node node = (Node) expr.evaluate(doc, XPathConstants.NODE);        
        this.nom = node.getTextContent(); 
        
        expr = XPathFactory.newInstance().newXPath().compile("/llibreria/poblacio");
        node = (Node) expr.evaluate(doc, XPathConstants.NODE);        
        this.poblacio = node.getTextContent();
        
        expr = XPathFactory.newInstance().newXPath().compile("/llibreria/llibres");
        NodeList llistaNodes = (NodeList) expr.evaluate(doc, XPathConstants.NODESET); 
        
        for (int i=0; i<llistaNodes.getLength(); i++) {
            expr = XPathFactory.newInstance().newXPath().compile("/llibreria/llibres/llibre["+(i+1)+"]");
            node = (Node) expr.evaluate(doc, XPathConstants.NODE);
            Llibre llibre = new Llibre();
            llibre.llegir(node);
            llibres.add(llibre);            
        }
        
    }
}
