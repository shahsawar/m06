package org.example;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

/**
 * Hello world!
 *
 */
public class Main
{
    public static void main( String[] args ) {

        try (MongoClient mongoClient = MongoClients.create()) {
            MongoDatabase database = mongoClient.getDatabase("bd_activitat_2_3");
            MongoCollection<Document> collection = database.getCollection("clients");
        }


    }

    public static void exercici1(MongoCollection collection){

    }
    public static void exercici2(MongoCollection collection){

    }
    public static void exercici3(MongoCollection collection){

    }
    public static void exercici4(MongoCollection collection){

    }

}
