import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

public class Assignatura {

    private String nom;
    private int hores_setmana;
    List<Alumne> alumnes_matriculats = new ArrayList<>();

    public Assignatura(String nom, int hores_setmana) {
        this.nom = nom;
        this.hores_setmana = hores_setmana;
    }

    public Assignatura(Document document) {
        this.nom = document.getString("nom");
        this.hores_setmana = document.getInteger("hores_setmana");

        if (document.containsKey("alumnes")){
            List<Document> listTmp = document.getList("alumnes", Document.class);
            for (Document documentTmp : listTmp) {
                alumnes_matriculats.add(new Alumne(documentTmp));
            }
        }
    }


    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getHores_setmana() {
        return hores_setmana;
    }

    public void setHores_setmana(int hores_setmana) {
        this.hores_setmana = hores_setmana;
    }

    //Convert to document
    public Document toDocument() {
        Document doc = new Document("nom", nom).append("hores_setmana", hores_setmana);
        if (!alumnes_matriculats.isEmpty()) {
            List<Document> documentList = new ArrayList<>();
            for (Alumne alumneTmp : alumnes_matriculats) {
                documentList.add(alumneTmp.toDocument());
            }
            doc.append("alumnes", documentList);
        }
        return doc;
    }


    // Convert Assignatura document list to Assignatura list
    public static List<Assignatura> loadIterable(Iterable<Document> iterable) {
        List<Assignatura> clientsTmp = new ArrayList<>();
        for (Document documentTmp : iterable) {
            Assignatura c = new Assignatura(documentTmp);
            clientsTmp.add(c);
        }
        return clientsTmp;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Assignatura {").append(System.lineSeparator());
        sb.append("\tnom: ").append(nom).append(System.lineSeparator());
        sb.append("\thores_setmana: ").append(hores_setmana).append(System.lineSeparator());
        sb.append("\tAlumnes: ").append(System.lineSeparator());
        for (Alumne a : alumnes_matriculats) {
            sb.append(a.toString());
        }
        sb.append("}");
        return sb.toString();
    }
}
