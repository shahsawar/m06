import org.bson.Document;

import javax.print.Doc;

public class Alumne {

    private String dni;
    private String nom;

    public Alumne(String dni, String nom) {
        this.dni = dni;
        this.nom = nom;
    }

    public Alumne(Document document){
        this.dni = document.getString("dni");
        this.nom = document.getString("nom");
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Document toDocument() {
        Document doc = new Document("dni", dni).append("nom", nom);
        return doc;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\t\t{dni: ").append(dni).append(", ");
        sb.append("\tnom: ").append(nom).append("}").append(System.lineSeparator());
        return sb.toString();
    }
}
