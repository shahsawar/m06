package m03_Activitat_3_1_ShahSawar;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

public class Llibre {
    String autor;
    String titol;
    
    
    public Llibre() {
    }
    
    public Llibre(String autor, String titol) {
        this.autor = autor;
        this.titol = titol;
    }
   
    public void imprimir() {
        System.out.println("Títol: "+this.titol);
        System.out.println("Autor: "+this.autor);
        System.out.println("---");
    }
    
    public void saveXml(Element llibres, Document doc) {
    	Element llibre = doc.createElement("llibre");
    	llibre.setAttribute("autor", autor);
    	llibre.setAttribute("titol", titol);
    	llibres.appendChild(llibre);
    }
    
    public void readXml(Node Llibre) {
    	NamedNodeMap atributs = Llibre.getAttributes();
    	this.titol = atributs.getNamedItem("titol").getTextContent();
    	this.autor = atributs.getNamedItem("autor").getTextContent();
    }
    
}
