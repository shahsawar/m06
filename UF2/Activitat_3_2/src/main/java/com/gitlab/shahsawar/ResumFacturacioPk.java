package com.gitlab.shahsawar;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class ResumFacturacioPk implements Serializable {

    private int mes;
    private int any;
    private int dni_client;

    public ResumFacturacioPk() {
        super();
    }

    public ResumFacturacioPk(int mes, int any, int dni_client) {
        this.mes = mes;
        this.any = any;
        this.dni_client = dni_client;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public int getAny() {
        return any;
    }

    public void setAny(int any) {
        this.any = any;
    }

    public int getDni_client() {
        return dni_client;
    }

    public void setDni_client(int dni_client) {
        this.dni_client = dni_client;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ResumFacturacioPk that = (ResumFacturacioPk) o;
        return mes == that.mes && any == that.any && dni_client == that.dni_client;
    }

    @Override
    public int hashCode() {
        return Objects.hash(mes, any, dni_client);
    }
}
