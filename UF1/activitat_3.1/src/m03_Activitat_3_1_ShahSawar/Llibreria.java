package m03_Activitat_3_1_ShahSawar;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class Llibreria {
    String nom;
    String poblacio;
    List<Llibre> llibres = new ArrayList<>();

    String rutaArxiu = "llibreria.xml";
    
    public Llibreria() {
       
    }
    
    public Llibreria(String nom, String poblacio) {
        this.nom = nom;
        this.poblacio = poblacio;
    }
    
    public void imprimir() {
        System.out.println("===== DADES DE LA LLIBRERIA ====");
        System.out.println("Nom: "+this.nom);
        System.out.println("Població: "+this.poblacio);
        System.out.println("Llibres:");
        for (Llibre llibre : this.llibres) {
            llibre.imprimir();
        }
        System.out.println("================================");
    }
    
    
    
    public void saveXml() throws ParserConfigurationException, TransformerConfigurationException, TransformerException {
    	Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
		
		//Element llibreria
		Element llibreria = doc.createElement("llibreria");
		doc.appendChild(llibreria);
		
		//Element nom
		Element nom = doc.createElement("nom");
		nom.setTextContent(this.nom);
		llibreria.appendChild(nom);
		
		//Element poblacio
		Element poblacio = doc.createElement("poblacio");
		poblacio.setTextContent(this.poblacio);
		llibreria.appendChild(poblacio);
		
		//Element llibres
		Element Llibres = doc.createElement("llibres");
		llibreria.appendChild(Llibres);
		for (Llibre llibre : llibres) {
			llibre.saveXml(Llibres, doc);
		}
		
		//Per desar la informació en un arxiu.
		TransformerFactory.newInstance().newTransformer().
         transform(new DOMSource(doc), new StreamResult(new File (rutaArxiu)));
	}
    
    
    public void readXml(String rutaArxiu) throws SAXException, IOException, ParserConfigurationException {
    	
    	Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(rutaArxiu);
        Node arrel = doc.getDocumentElement();
        NodeList llistaNodes = arrel.getChildNodes();

		for (int i = 0; i < llistaNodes.getLength(); i++) {

			// si és nom
			if (llistaNodes.item(i).getNodeName().equals("nom")) {
				nom = llistaNodes.item(i).getTextContent();
			}

			// si és poblacio
			if (llistaNodes.item(i).getNodeName().equals("poblacio")) {
				poblacio = llistaNodes.item(i).getTextContent();
			}
			
			// si són llibres
			if (llistaNodes.item(i).getNodeName().equals("llibres")) {
				
				NodeList llistaLlibres = llistaNodes.item(i).getChildNodes();
				for (int j=0; j<llistaLlibres.getLength(); j++) {
                    Llibre llibre = new Llibre();
                    llibre.readXml(llistaLlibres.item(j));
                    llibres.add(llibre);
                }
			}
		}
    }
    
    
    
    
}

