public abstract class GestorPersistencia {

    public abstract void inserBiblioteca(Biblioteca biblioteca);
    public abstract void deleteBiblioteca(Biblioteca biblioteca);
    public abstract void updateBiblioteca(Biblioteca biblioteca);
    public abstract void getCodiPostalBiblioteca(Biblioteca biblioteca);

    public abstract void inserLlibre(Llibre Llibre);
    public abstract void deleteLlibre(Llibre Llibre);
    public abstract void updateLlibre(Llibre Llibre);
    public abstract void findByNameLlibre(Llibre Llibre);
    public abstract void rentBookLlibre(Llibre Llibre);
}
