/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hibernatetest;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author shah
 */
@Entity
@Table(name = "person")
public class Person implements Serializable{
    
    @Id
    String dni;
    
    @Column
    String nom;
    
    @Column
    int edat;

    @Override
    public String toString() {
        return "Person{" + "dni=" + dni + ", nom=" + nom + ", edat=" + edat + '}';
    }
    
    
}
