USE lamevabbdd;
-- ALTER TABLE persona ADD menor ENUM('S','N');

DROP PROCEDURE IF EXISTS omple_menors; 
DELIMITER // 
CREATE PROCEDURE omple_menors() BEGIN 
DECLARE v_dni varchar(10);
DECLARE v_data_naixement DATE;
DECLARE v_menor ENUM('S','N'); 
DECLARE acaba INT DEFAULT FALSE; 

DECLARE c_persones CURSOR FOR SELECT dni, data_naixement FROM persona; 
DECLARE CONTINUE HANDLER FOR NOT FOUND SET acaba = TRUE;
OPEN c_persones; 
read_loop: LOOP 
    FETCH c_persones INTO v_dni, v_data_naixement; 
    IF acaba THEN
        LEAVE read_loop;
    END IF;
    IF v_data_naixement > CURRENT_DATE() - INTERVAL 18 YEAR THEN
        SET v_menor = 'S';
    ELSE
        SET v_menor = 'N';
    END IF;
    UPDATE persona SET menor = v_menor WHERE dni = v_dni;
END LOOP; 
CLOSE c_persones; 
END; 
//

call omple_menors();