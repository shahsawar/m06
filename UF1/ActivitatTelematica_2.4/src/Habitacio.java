import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

public class Habitacio {

	protected double ample;
	protected double llarg;
	protected String nom;
	protected List<Moble> mobles = new ArrayList<>();

	public Habitacio() {
		// Llegir dades d'habitació d'l'arxiu.
		Properties p = new Properties();
		try (DataInputStream ip = new DataInputStream(new FileInputStream("dadesHabitacio.properties"))) {
			p.load(ip);
			ample = Double.parseDouble(p.getProperty("Ample"));
			llarg = Double.parseDouble(p.getProperty("Llarg"));
			nom = p.getProperty("Nom");
		} catch (IOException e) {
			e.printStackTrace();
		}

		// Si l'arxiu existeix, omplirà la llista amb els valors de l'arxiu, en canvi
		// crearà una habitació buida.
		if (fitxerExisteix()) {
			llegirFitxer();
		}
	}

	// Afegir un moble a l'habitació
	public void afegirMoble() {
		Scanner sc = new Scanner(System.in);

		System.out.println("Introdueix el ample: ");
		double ample = sc.nextDouble();
		System.out.println("Introdueix el llarg: ");
		double llarg = sc.nextDouble();
		System.out.println("Introdueix el color: ");
		int color = sc.nextInt();

		Moble mobleTmp = new Moble(ample, llarg, color);
		mobles.add(mobleTmp);
	}

	// Guardar a disc l'habitació
	public void guardarHabitacio() {
		try (DataOutputStream sortida = new DataOutputStream(
				new BufferedOutputStream(new FileOutputStream("/home/shahsawar/test")))) {

			// Guardar la longitud de la llista en la primera posició de l'arxiu.
			sortida.write(mobles.size());

			for (Moble moble : mobles) {
				sortida.writeDouble(moble.ample);
				sortida.writeDouble(moble.llarg);
				sortida.writeInt(moble.color);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// Llistar per pantalla tots els mobles de l'habitació, amb totes les seves
	// dades.
	public void llistarMobles() {

		for (Moble moble : mobles) {
			System.out.println(
					"Ample: " + moble.ample + "\n" + "Llarg: " + moble.llarg + "\n" + "Color: " + moble.color + "\n");
		}
	}

	// Comprovar si arxiu existeix o no.
	public boolean fitxerExisteix() {
		File file = new File("moblesHabitacio");
		if (file.exists()) {
			return true;
		}
		return false;
	}

	// Omplir l'habitació amb mobles
	public void llegirFitxer() {

		try (DataInputStream sortida = new DataInputStream(
				new BufferedInputStream(new FileInputStream("/home/shahsawar/test")))) {

			// Recuperar la longitud de la llista
			int length = sortida.read();

			for (int i = 0; i < length; i++) {

				double ampleTmp = sortida.readDouble();
				double llargTmp = sortida.readDouble();
				int colorTmp = sortida.readInt();

				Moble mobleTmp = new Moble(ampleTmp, llargTmp, colorTmp);
				mobles.add(mobleTmp);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	// Mostrar dades de Habitació
	public void dadesHabitacio() {
		System.out.println("Dades de habitació:\n" + "Ample : " + ample + "\n" + "Llarg : " + llarg + "\n" + "Nom : "
				+ nom + "\n");
	}

	/*
	 * //Creació d'arxiu de propietat i guardar els valors predeterminats. 
	 * protected void createPropertiesFile() {
	 * 
	 * Properties p = new Properties();
	 * 
	 * try (DataOutputStream op = new DataOutputStream(new
	 * FileOutputStream("dadesHabitacio.properties"))){
	 * 
	 * p.setProperty("Ample", "200"); p.setProperty("llarg", "200");
	 * p.setProperty("Nom", "N2I");
	 * 
	 * p.store(op, null);
	 * 
	 * } catch (IOException e) { e.printStackTrace(); } }
	 */
}
