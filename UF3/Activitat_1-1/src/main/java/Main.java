import com.mongodb.client.*;
import com.mongodb.client.result.DeleteResult;
import org.bson.Document;

import java.util.logging.Level;

import static com.mongodb.client.model.Filters.gt;

public class Main {
    public static void main(String[] args) {

        java.util.logging.Logger.getLogger("org.mongodb").setLevel(Level.WARNING);

        //Main menu
        MenuBuilder menu = new MenuBuilder();
        menu.addTitle("M06", true);
        menu.addEmptyLine();
        menu.addLine("1. Add Assignatures.", false);
        menu.addLine("2. Find Assignatures.", false);
        menu.addLine("3. Delete All Assignatures.", false);
        menu.addLine("0. Exit", false);
        menu.addSeparator();


        try (MongoClient mongoClient = MongoClients.create();) {
            MongoDatabase database = mongoClient.getDatabase("bd_prova");
            MongoCollection<Document> collection = database.getCollection("assignatures");

            //Menu
            while (menu.isRunning()){
                menu.print();

                switch (menu.askOption()){
                    case 1:
                        exercici1(collection);
                        menu.waitForInput();
                        break;
                    case 2:
                        exercici2(collection);
                        menu.waitForInput();
                        break;
                    case 3:
                        exercici3(collection);
                        menu.waitForInput();
                        break;
                    case 0:
                        menu.requestStop();
                        break;
                }
            }
        }
    }

    public static void exercici1(MongoCollection<Document> collection){
        collection.insertOne(new Assignatura("Accés a dades", 5).toDocument());
        collection.insertOne(new Assignatura("Sistemes de gestió empresarial", 3).toDocument());
    }

    public static void exercici2(MongoCollection<Document> collection){
        try (MongoCursor<Document> cursor
                     = collection.find(gt("hores_setmana", 2)).iterator()){
            while (cursor.hasNext()){
                Assignatura assignatura = new Assignatura(cursor.next());
                System.out.println(assignatura);
            }
        }
    }

    public static void exercici3(MongoCollection<Document> collection){
        DeleteResult deleteResult = collection.deleteMany(gt("hores_setmana", 0));
    }

}
