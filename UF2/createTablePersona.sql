use lamevabbdd;
-- show tables;
drop table persona;
create table persona(dni varchar(10) primary key, data_naixement DATE, menor enum('S', 'N'));
insert into persona(dni, data_naixement) values ('12345671', STR_TO_DATE('10-10-2000', '%d-%m-%Y'));
insert into persona(dni, data_naixement) values ('12345672', STR_TO_DATE('10-10-2003', '%d-%m-%Y'));
insert into persona(dni, data_naixement) values ('12345673', STR_TO_DATE('01-01-2001', '%d-%m-%Y'));
commit;

-- delete from persona where dni='12345678';
-- rollback;