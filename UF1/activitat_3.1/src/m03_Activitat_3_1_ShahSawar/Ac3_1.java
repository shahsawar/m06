package m03_Activitat_3_1_ShahSawar;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;

import org.xml.sax.SAXException;

public class Ac3_1 {

    public static void main(String[] args) throws SAXException, IOException, ParserConfigurationException, TransformerConfigurationException, TransformerException {
        
    	/*
    	Llibreria casa = new Llibreria("Casa del llibre", "Vic");
        Llibre llib1 = new Llibre("El quadern gris","Josep Pla");
        Llibre llib2 = new Llibre("El perquè de tot plegat","Quim Monzó");
        casa.llibres.add(llib1);
        casa.llibres.add(llib2);
        casa.saveXml();
        casa.imprimir();
        */
    	
    	Llibreria l = new Llibreria();
    	l.readXml("llibreria.xml");
    	l.imprimir();
    	
    }
}
