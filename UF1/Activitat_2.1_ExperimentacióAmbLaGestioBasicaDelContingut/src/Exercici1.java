import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Exercici1 {

	public static void main(String[] args) {

		// Ruta
		//String ruta = "/home/users/inf/wiam2/iam2318925/TestM06/exercici1";
		String ruta = "/home/shahsawar/Documents/Entregas/testM06/file";
		
		try {
			DataOutputStream streamEscriptura = new DataOutputStream(
					new BufferedOutputStream(new FileOutputStream(ruta)));
			DataInputStream streamLectura = new DataInputStream(new BufferedInputStream(new FileInputStream(ruta)));

			// Escriptura
			streamEscriptura.writeBoolean(true);
			streamEscriptura.writeInt(1000000);
			streamEscriptura.writeUTF("Proba ejercici1");
			streamEscriptura.close();

			// Lectura
			boolean valor1 = streamLectura.readBoolean();
			int valor2 = streamLectura.readInt();
			String valor3 = streamLectura.readUTF();

			System.out.println("El valor booleano: "+valor1);
			System.out.println("El valor entero: "+valor2);
			System.out.println("Conjunto de caracteres: "+valor3);

		} catch (IOException e) {
			e.printStackTrace();
		}

		/*
		 * Observa el contingut del fitxer. Per què creus que es mostra així?
		 * Perquè que el contingut de l'arxiu està emmagatzemat com bytes.
		 */

		/*
		 * Què creus que passaria si, on hi ha un booleà, intentéssim recuperar un
		 * enter? Fes-ne la prova i digues què passa.
		 * Em dóna error al compilar. Perquè estic passant un nombre enter a un 
		 * mètode que sol permet rebre booleà com a paràmetre.
		 */

	}

}
