import java.util.Scanner;

public class Main {
	
	public static int menu() {
		Scanner sc = new Scanner(System.in);
		System.out.println(
				"0 - Sortir\n"+
				"1 - Mostrar per pantalla les dades de l'habitació\n"+
				"2 - Afegir un moble a l'habitació\n"+
				"3 - Guardar a disc l'habitació\n"+
				"4 - Llistar per pantalla tots els mobles de l'habitació, amb totes les seves dades: \n");
		int op = sc.nextInt();
		return op;
	}

	public static void main(String[] args) {

		Habitacio h = new Habitacio();

		int option = menu();
		while (option != 0) {

			switch (option) {
			case 1:
				h.dadesHabitacio();
				break;

			case 2:
				h.afegirMoble();
				break;

			case 3:
				h.guardarHabitacio();
				break;

			case 4:
				h.llistarMobles();
				break;

			default:
				System.err.println("Aquesta opció no és vàlida");
				break;
			}
			option = menu();
		}
		System.out.println("Surt del programa.");
	}

}
