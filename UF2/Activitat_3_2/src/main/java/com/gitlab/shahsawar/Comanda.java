package com.gitlab.shahsawar;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

@Entity
@Table(name = "comanda")
public class Comanda implements Serializable {

    @Id
    private int num_comanda;
    private double preu_total;
    private Date data;
    private int dni_client;

    public Comanda() {
    }

    public Comanda(int num_comanda, double preu_total, Date data, int dni) {
        this.num_comanda = num_comanda;
        this.preu_total = preu_total;
        this.data = data;
        this.dni_client = dni;
    }

    public int getNum_comanda() {
        return num_comanda;
    }

    public void setNum_comanda(int num_comanda) {
        this.num_comanda = num_comanda;
    }

    public double getPreu_total() {
        return preu_total;
    }

    public void setPreu_total(double preu_total) {
        this.preu_total = preu_total;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public int getDni_client() {
        return dni_client;
    }

    public void setDni_client(int dni_client) {
        this.dni_client = dni_client;
    }

    @Override
    public String toString() {
        return "Comanda{" +
                "num_comanda=" + num_comanda +
                ", preu_total=" + preu_total +
                ", data=" + data +
                ", dni_client=" + dni_client +
                '}';
    }

    // Add order in database
    public static void add(SessionFactory factory, Client client) {
        Comanda comandaTmp = createComand(client);

        if (!client.comandes.contains(comandaTmp)) {
            try (Session session = factory.openSession()) {
                session.beginTransaction();
                session.save(comandaTmp);
                session.getTransaction().commit();
                System.out.println("Order Successfully Created.");
            } catch (Exception e) {
                System.err.println("The order already exists!\n");
                e.getStackTrace();
            }
            client.comandes.add(comandaTmp);
        } else {
            System.out.println("The comanda already exists!");
        }
    }

    // Create new order
    private static Comanda createComand(Client c) {

        System.out.print("Enter a order id: ");
        int dni = Client.getScanner().nextInt();
        System.out.print("Enter order price: ");
        Double preu = Client.getScanner().nextDouble();
        System.out.print("Enter a order date (format: yyyy-MM-dd): ");
        Date date = MainHibernate.setDate(Client.getScanner().next());

        return new Comanda(dni, preu, date, c.getDni());
    }

}
