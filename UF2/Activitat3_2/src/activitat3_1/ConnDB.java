package activitat3_1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class ConnDB {

	static Connection con;
	
	public static Connection startConn(){
		
		// Dades de connexió de base de dades.
		String url="jdbc:mysql://localhost/lamevabbdd";
		String usuari="lamevaapp";
		String password="lamevaapp";
		
		// Iniciar la connexió a la base de dades
		try {
			con = (Connection) DriverManager.getConnection(url, usuari, password);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return con;
	}
	
	// Tancar la connexió a la base de dades.
	public static void closeConn() {
		try {
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
        
        
        //Connexió a la base de dades con Hibernate
        public static void startHibernateConn(){
            
            Configuration configuration = new Configuration();
            configuration.configure();
            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties()).build();
            SessionFactory factory = configuration.buildSessionFactory(serviceRegistry);
            
            
        }
        
        //Tancar la connexió a la base de dades (Hibernate).
        public static void closeHibernateConn(SessionFactory factory, ServiceRegistry serviceRegistry){
            factory.close();
            StandardServiceRegistryBuilder.destroy(serviceRegistry);
        }
}
