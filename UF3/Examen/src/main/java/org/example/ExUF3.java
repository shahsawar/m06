
package org.example;

import com.mongodb.client.*;
import com.mongodb.client.model.Filters;
import org.bson.Document;
import org.bson.types.ObjectId;

import javax.print.Doc;
import java.util.ArrayList;
import java.util.List;

public class ExUF3 {

    public static void main(String[] args) {
        NomComercial renault_clio = new NomComercial("Renault","Clio");
        NomComercial bmw_golf = new NomComercial("BMW","Golf");
        Revisio r1 = new Revisio(2018,true,"Sense incidències");
        Revisio r2 = new Revisio(2019,true,"Sense incidències");
        Revisio r3 = new Revisio(2020,true,"Retrovisor dret amb mala visibilitat");
        Revisio r4 = new Revisio(2021,false,"Presió dels pneumàtics insuficient");
        Cotxe c1 = new Cotxe("1234ABC",300,renault_clio);
        c1.revisions.add(r1);
        c1.revisions.add(r2);
        Cotxe c2 = new Cotxe("5678DEF",300,bmw_golf);
        c2.revisions.add(r2);
        c2.revisions.add(r3);
        c2.revisions.add(r4);

        c1.nom_comercial = renault_clio;

        //exercici1(c1);
        //exercici1(c2);

        List<Cotxe> llista_cotxes = exercici2("BMW");
        for (Cotxe c: llista_cotxes) {
            System.out.println(c);
        }
    }

    public static void exercici1 (Cotxe cotxe) {
        try (MongoClient mongoClient = MongoClients.create()) {
            MongoDatabase database = mongoClient.getDatabase("provaUF3");
            MongoCollection<Document> collection = database.getCollection("Cotxes");

            //Document Car
            Document docCar = new Document();
            docCar.append("matricula", cotxe.matricula);
            docCar.append("cavalls", cotxe.cavalls);

            //Document Comercial
            Document docComercial = new Document();
            docComercial.append("marca", cotxe.nom_comercial.marca);
            docComercial.append("model", cotxe.nom_comercial.model);
            docCar.append("nom_comercial", docComercial);

            //Document Revisio
            if (!cotxe.revisions.isEmpty()) {
                List<Document> revisionsList = new ArrayList<>();
                for (Revisio revisio : cotxe.revisions) {
                    Document docRevicio = new Document();
                    docRevicio.append("any", revisio.any);
                    docRevicio.append("favorable", revisio.favorable);
                    docRevicio.append("comentaris", revisio.comentaris);
                    revisionsList.add(docRevicio);
                }
                docCar.append("revisions", revisionsList);
            }
            collection.insertOne(docCar);
        }
    }

    public static List<Cotxe> exercici2 (String marca) {
        List<Cotxe> llista_cotxes = new ArrayList<>();
        /* TODO: Recuperar d'una BBDD noSQL (Mongo) la llista de cotxes */
        /* Tan sols recuperarem els cotxes de la marca especificada que tinguin almenys 3 revisions */;

        MongoClient mongoClient = MongoClients.create();
        MongoDatabase database = mongoClient.getDatabase("provaUF3");
        MongoCollection<Document> collection = database.getCollection("Cotxes");
        MongoCursor<Document> cursor = collection.find(Filters.gt("revisonsz", marca))).iterator();

        try {
            while (cursor.hasNext()) {
                Document d = new Document(cursor.next());
                String matricula = d.getString("matricula");
                int cavalls = d.getInteger("cavalls");

                NomComercial nomComercialTmp = d.get("nom_comercial");

                List<Revisio> revisiosTmp = new ArrayList<>();
                if (d.containsKey("revisions")) {
                    List<Document> listTmp = d.getList("revisions", Document.class);
                    for (Document documentTmp : listTmp) {
                        int any = documentTmp.getInteger("any");
                        boolean favorable = documentTmp.getBoolean("favorable");
                        String comentaris = documentTmp.getString("comentaris");
                        revisiosTmp.add(new Revisio(any, favorable, comentaris));
                    }
                }
                Cotxe cotxeTmp = new Cotxe(matricula, cavalls, nomComercialTmp);
                cotxeTmp.revisions.addAll(revisiosTmp);
                llista_cotxes.add(cotxeTmp);
            }
        }finally {
            cursor.close();
            mongoClient.close();
        }

        return llista_cotxes;
    }

    /*
    //Exercici4
    db.cotxes.find({$and[{"cavalles" : {$gt : 100}},{"revisions.any":2019}]})

    //exercici4
    db.cotxes.find({$and:[{"nom_comercial.marca":"BMW"},{"nom_comercial.marca":"Renault"}]})
*/

}
