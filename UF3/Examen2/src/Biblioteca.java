import java.util.ArrayList;
import java.util.List;

public class Biblioteca {

    private int codi;
    private String nom;
    private int codiPostal;
    List<Llibre> llibreList = new ArrayList<>();

    public Biblioteca(int codi, String nom, int codiPostal, List<Llibre> llibreList) {
        this.codi = codi;
        this.nom = nom;
        this.codiPostal = codiPostal;
        this.llibreList = llibreList;
    }

    public int getCodi() {
        return codi;
    }

    public void setCodi(int codi) {
        this.codi = codi;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getCodiPostal() {
        return codiPostal;
    }

    public void setCodiPostal(int codiPostal) {
        this.codiPostal = codiPostal;
    }

    public List<Llibre> getLlibreList() {
        return llibreList;
    }

    public void setLlibreList(List<Llibre> llibreList) {
        this.llibreList = llibreList;
    }
}
