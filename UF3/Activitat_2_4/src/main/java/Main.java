import com.mongodb.client.*;
import com.mongodb.client.model.Filters;
import org.bson.Document;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;

import static com.mongodb.client.model.Filters.eq;


public class Main {

    public static void main(String[] args) throws ParseException {

        java.util.logging.Logger.getLogger("org.mongodb").setLevel(Level.WARNING);
        Scanner sc = new Scanner(System.in);

        try (MongoClient mongoClient = MongoClients.create()) {
            MongoDatabase database = mongoClient.getDatabase("bd_activitat_2_3");
            MongoCollection<Document> collection = database.getCollection("clients");

            Boolean salir = false;
            while (!salir){
                menu();
                int option = sc.nextInt();
                switch (option){
                    case 0:
                        salir=true;
                        break;
                    case 1:
                        exercici2(collection, sc);
                        break;
                    case 2:
                        exercici4(collection, sc);
                        break;
                    case 3:
                        exercici5(collection, sc);
                        break;
                    case 4:
                        exercici6(collection, sc);
                        break;
                    default:
                        System.err.println("This option does not exist!");
                }
            }
        }
    }


    // Insert the client into the database
    public static void exercici1(MongoCollection<Document> collection) {
        collection.insertOne(new Client("12345678X", "Pere Pons", 0).toDoc());
    }


    // Insert the client read by the console in the database
    public static void exercici2(MongoCollection<Document> collection, Scanner sc) {
        collection.insertOne(readClient(sc).toDoc());
    }


    // Show all clients in the database.
    public static List<Client> exercici3(MongoCollection<Document> collection) {
        List<Client> clientsList = Client.loadIterable(collection.find());
        int cont = 1;
        for (Client c : clientsList) {
            System.out.println(cont + ") " + c.getNif());
            cont++;
        }
        return clientsList;
    }


    // Insert new comanda in client
    public static void exercici4(MongoCollection<Document> collection, Scanner sc) {

        //Display all clients on screen
        List<Client> clients = exercici3(collection);

        //Read client from keyboard
        int numClient;
        do {
            System.out.print("Select the client : ");
            numClient = sc.nextInt();
        } while (numClient > clients.size() || numClient == 0);

        //Add a new comanda in client and update totalFacturacio variable
        Client clientSelected = clients.get(numClient - 1);
        Comanda comandaTmp = null;
        try {
            comandaTmp = readComanda(sc);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        clientSelected.comandes.add(comandaTmp);
        int incrementTotalFacturacio = clientSelected.getTotalFacturacio() + comandaTmp.getImporte();
        clientSelected.setTotalFacturacio(incrementTotalFacturacio);

        // remove the old client from the database and insert the new one
        collection.deleteOne(eq("nif", clientSelected.getNif()));
        collection.insertOne(clientSelected.toDoc());
    }


    // Search for clients based on their total facturació.
    public static void exercici5(MongoCollection<Document> collection, Scanner sc) {
        System.out.print("Look for clients with total facturació greater than: ");
        int num = sc.nextInt();
        try (MongoCursor<Document> cursor = collection.find(Filters.gt("total_facturacio", num)).iterator()) {
            while (cursor.hasNext()) {
                Client client = new Client(cursor.next());
                System.out.println(client.toString());
            }
        }
    }


    // Look for clients based on their comandes quantity
    public static void exercici6(MongoCollection<Document> collection, Scanner sc) {
        System.out.print("Look for clients with comandes greater than: ");
        int num = sc.nextInt();
        List<Client> clientsList = Client.loadIterable(collection.find());
        for (Client c : clientsList) {
            if (c.comandes.size() >= num) {
                System.out.println(c.toString());
            }
        }
    }


    // While the required field is empty it will ask for the.
    public static String validString(Scanner sc, String q) {
        String answerTmp;
        do {
            System.out.print(q);
            answerTmp = sc.next();
        } while (answerTmp.isBlank());

        return answerTmp;
    }


    public static Client readClient(Scanner sc) {
        System.out.println("Enter client data:");
        String nifTmp = validString(sc, "NIF: ");
        String nomTmp = validString(sc, "NOM: ");
        System.out.print("Total Facturacio: ");
        int totalFacturacioTmp = sc.nextInt();
        sc.nextLine();

        Client clientTmp = new Client(nifTmp, nomTmp, totalFacturacioTmp);

        // If the optional fields are empty, we do not create them in the database
        System.out.print("Telèfon: ");
        String telefonTmp = sc.nextLine();
        System.out.print("Correu: ");
        String correuTmp = sc.nextLine();

        if (!telefonTmp.isBlank()) {
            clientTmp.setTelefon(telefonTmp);
        }
        if (!correuTmp.isBlank()) {
            clientTmp.setCorreu(correuTmp);
        }
        return clientTmp;
    }


    public static Comanda readComanda(Scanner sc) throws ParseException {
        System.out.println("Enter comanda data: ");
        String dataString = validString(sc, "Date(dd-mm-yyyy): ");
        System.out.print("Import: ");
        int importe = sc.nextInt();
        System.out.print("Pagada(true/false): ");
        Boolean pagada = sc.nextBoolean();

        Comanda comandaTmp = new Comanda(new SimpleDateFormat("dd-MM-yyyy").parse(dataString), importe, pagada);
        return comandaTmp;
    }


    public static void menu(){
        StringBuffer sb = new StringBuffer();
        sb.append(System.lineSeparator());
        sb.append("0) ").append("Sortir").append(System.lineSeparator());
        sb.append("1) ").append("Donar d'alta un client").append(System.lineSeparator());
        sb.append("2) ").append("Afegir comandes a un client").append(System.lineSeparator());
        sb.append("3) ").append("Cercar clients per facturació").append(System.lineSeparator());
        sb.append("4) ").append("Cercar clients per quantitat de comandes").append(System.lineSeparator());
        System.out.print(sb+":");
    }


}
