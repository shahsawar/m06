import java.io.Serializable;

@SuppressWarnings("serial")
public class Treballador implements Serializable{

	protected String NIF;
	protected String Nom;
	protected int Sou;
	
	protected Treballador() {
		NIF = null;
		Nom = null;
		Sou = 0;
	};
	
	protected Treballador(String nIF, String nom, int sou) {
		NIF = nIF;
		Nom = nom;
		Sou = sou;
	}
}
